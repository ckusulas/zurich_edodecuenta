<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdf_Controller extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->library('Pdf_Library');
        $this->load->model('Pdf_Model');
        $this->load->model('Extract_data_model');
    }


    public function generate_pdf_report(){

        $data['clients'] = $this->Pdf_Model->select_clients();
        $this->load->view('clients_report',$data);
    }

    public function generate_pdf_report_images(){

       // $data['clients'] = $this->Pdf_Model->select_clients();
        $this->load->view('images_report');
    }

    public function generate_pdf_report_images2(){

        // $data['clients'] = $this->Pdf_Model->select_clients();
         $this->load->view('images_report2');
     }


    public function generate_pdf_report_images3(){

        // $data['clients'] = $this->Pdf_Model->select_clients();
         $this->load->view('images_report3');
     }

     public function generate_pdf_report_images4(){

        // $data['clients'] = $this->Pdf_Model->select_clients();
         $this->load->view('images_report4');
     }

     public function generate_pdf_report_images5(){

        // $data['clients'] = $this->Pdf_Model->select_clients();
         $this->load->view('images_report5');
     }
     
     public function generate_pdf_report_images6(){

        // $data['clients'] = $this->Pdf_Model->select_clients();
         $this->load->view('images_report6');
     }

         
     public function generate_pdf_report_images7(){

        // $data['clients'] = $this->Pdf_Model->select_clients();
         $this->load->view('images_report7');
     }


     public function generate_pdf_report_images8(){

        // $data['clients'] = $this->Pdf_Model->select_clients();
         $this->load->view('images_report8');
     }

     public function generate_EdoCta($noReporte){

        // $data['clients'] = $this->Pdf_Model->select_clients();
        if($noReporte==""){
            $this->load->view('edoCta_view');
        }

        if($noReporte == "13129"){
            $this->load->view('edoCta_view_13129');
        }

        if($noReporte == "12890"){
            $this->load->view('edoCta_view_12890');
        }
     }

 

     public function fromXLXs($poliza="12890"){

      

        $data = $this->Extract_data_model->select_poliza($poliza);

 
 
        $campos = $data->result_array()[0];

 

        $data = ($data->result_array()[0]); //->result_array()[0];


      

     


        $this->load->view('edoCta_view_plantilla_xlsx', $data);

    
     }




     public function fetch2($poliza="12890",$ffinicio="2019-09-30",$ffinal="2019-12-31",$display="no"){


 
        
        $msgError = $this->validaContenido($poliza);

        if($msgError!=""){
            echo $msgError;
            die();
        }else{
           

 
      

        $data = $this->Extract_data_model->select_poliza_view($poliza);

        
 
        $campos = $data->result_array()[0]; 

        //ramo
        $ramo = $campos['Ramo'];

        
 

   
    $campos['Producto'] = "OBJETIVO PROTEGIDO SANTANDER";

    if($ramo=="102"){
   
        $campos['Producto'] = "UNIT LINKED SANTANDER";
    }


    

    
       $datosFechaEmision =  $this->Extract_data_model->getDate_Emision($poliza);

       $campos2 = $datosFechaEmision->result_array();
       $datosE =  $campos2[0];
       $f_emision = $datosE['DISSUEDAT'];
 
       $datosPoliza = array("finicio" => format_dates($ffinicio), "ffinal" =>format_dates($ffinal), "fechaEmision" => format_dates($f_emision), "display"=>$display);

        $data = ($campos); //->result_array()[0];

        

        $datosEjecutivo =  $this->Extract_data_model->getData_Ejecutivo($poliza);

        $campos2 = $datosEjecutivo->result_array();
        $datosEjecutivoA =  $campos2[0];

        //print_r($campos2[0]); die();


    
        $detalleMovimiento = $this->getDataMovimientoPeriodo($poliza, $ffinicio, $ffinal);



       

        $arrayTitulosExistentes = $this->getListFondos($poliza,$ffinal);


         
       

        $datosTitulo = $this->getPrecioTitulo($ffinicio, $ffinal, $arrayTitulosExistentes); 

        $datosAlternativaRendim = $this->getCalculo($poliza, $ffinicio, $ffinal, $arrayTitulosExistentes);

       // print_r($datosAlternativaRendim); die();


         

        

        $data['datosEjecutivo'] = $datosEjecutivoA;


        $data['datosPoliza'] = $datosPoliza;
        $data['calculo_alternativa_rendim'] = $datosAlternativaRendim;
        $data['datos_titulo'] = $datosTitulo;


        $data['detalle_movimiento_periodo'] = $detalleMovimiento;

       //  print_r($data); die();
        
        
        
        if($ramo == 102){
            $this->load->view('edoCta_view_plantilla_102', $data);

        }else{
            $this->load->view('edoCta_view_plantilla_100', $data);
        }


        }
 
     }


     public function getDataMovimientoPeriodo($poliza, $finicio, $final){

        $data = $this->Extract_data_model->getDataPolizaBetween($poliza, $finicio, $final);
 
        $dataContent = $data->result_array();

        
        return($dataContent); 
 

     }


     public function reImprimirLogo(){
/*


base_url()."images/zurich-logo.jpg"

https://stackoverflow.com/questions/19918197/copy-a-file-to-another-directory-in-which-the-file-does-not-exist-php
        $file = BASEPATH.'/uploaded_images').'/'.$myAddr[0];
        if (!copy($file, $newfile)) {
            echo "failed to copy $file...\n";
        }*/

     }


     public function email(){

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'constantinokv@gmail.com',
            'smtp_pass' => 'santi2012####',
            'mailtype'  => 'text', //html
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        
        // Set to, from, message, etc.
        
        //$result = $this->email->send();


        $this->email->from('ckusulas@gmail.com', 'Constantino Kusulas');
        $this->email->to('constantinokv@gmail.com'); 

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');  

        $this->email->send(); 


     }


      


     function validaContenido($poliza){

        $msgError = "";

       // echo "tamos fcn validaContenido..";

        //int4310220191230  -   NPOLICY
        //int4310320191230  -   POLIZA
        //int4310420191230_rocio - POLIZA
        //tbl_calculos - POLIZA
        //tbl_calculos  column ref0
        //tbl_mail_anton - NPOLICY  
        //tbl_fondos_101 - POLIZA

        //Ejecutivo x
        //Contratante x
        //Cobranza x
        //Calculo x
        //Calculo_ref0 
        //tabla_anton
        //tabla_fondos_101



          //Contratantes
          $exist_tbl_contratante = $this->Extract_data_model->validateContent("int4310220191230","NPOLICY",$poliza);

          if($exist_tbl_contratante){
              $msgError.="FALTA CONTRATANTES(INT102), ";
          }


        //Ejecutivos
        $exist_tbl_ejecutivos = $this->Extract_data_model->validateContent("int4310320191230","POLIZA",$poliza);

        if($exist_tbl_ejecutivos){
            $msgError.="FALTA EJECUTIVOS(INT103), ";
        }

         //Cobranza
         $exist_tbl_contratante = $this->Extract_data_model->validateContent("int4310420191230_rocio","POLIZA",$poliza);

         if($exist_tbl_contratante){
             $msgError.="FALTA COBRANZA(INT104), ";
         }


        //Calculo
        $exist_tbl_calculos = $this->Extract_data_model->validateContent("tbl_calculos","POLIZA",$poliza);

        if($exist_tbl_calculos){
            $msgError.="FALTA CALCULO(INT105), ";
        }


          //tabla_anton
          $exist_tbl_calculos = $this->Extract_data_model->validateContent("tbl_mail_anton","NPOLICY",$poliza);

          if($exist_tbl_calculos){
              $msgError.="FALTA tabla_mail_anton, ";
          }



          //tabla_fondos 101
          $exist_tbl_calculos = $this->Extract_data_model->validateContent("tbl_fondos_101","POLIZA",$poliza);

          if($exist_tbl_calculos){
              $msgError.="FALTA TABLA FONDOS(INT101), ";
          }


          if($msgError!=""){
              $msgError = "Error:" . $msgError;
          }



     
        return($msgError);



     }

 
     function getCalculo($poliza, $finicio, $ffinal, $arrayTitulosActual){



        $memoryCalculoDataTitulo = array();

        $data = $this->Extract_data_model->getDataPoliza($poliza);


        foreach($arrayTitulosActual as $row){
            $memoryCalculoDataTitulo[$row['titulo']] = array();
            $memoryCalculoDataTitulo[$row['titulo']]['venta_titulo'] = 0;
            $memoryCalculoDataTitulo[$row['titulo']]['comodin'] = 0;
            $memoryCalculoDataTitulo[$row['titulo']]['aporte'] = 0;
      
            $memoryCalculoDataTitulo[$row['titulo']]['TAR_ingresos'] = 0;
            $memoryCalculoDataTitulo[$row['titulo']]['TAR_costo_seguro'] = 0;
            $memoryCalculoDataTitulo[$row['titulo']]['TAR_otros_egresos'] = 0;

            $memoryCalculoDataTitulo[$row['titulo']]['SAR_ingresos'] = 0;
            $memoryCalculoDataTitulo[$row['titulo']]['SAR_costo_seguro'] = 0;
            $memoryCalculoDataTitulo[$row['titulo']]['SAR_otros_egresos'] = 0;

            $memoryCalculoDataTitulo[$row['titulo']]['corte_inicio'] = 0;
            $memoryCalculoDataTitulo[$row['titulo']]['corte_final'] = 0;


            $memoryCalculoDataTitulo[$row['titulo']]['bandFirst'] = 0;

            $memoryCalculoDataTitulo[$row['titulo']]['titulo_cierre'] = 0;

            $memoryCalculoDataTitulo[$row['titulo']]['aporte_fecha'] = "";
            $memoryCalculoDataTitulo[$row['titulo']]['aporte_monto'] = 0;
            $memoryCalculoDataTitulo[$row['titulo']]['aporte_adicional'] = 0;
            $memoryCalculoDataTitulo[$row['titulo']]['retiros'] = 0;


            $memoryCalculoDataTitulo[$row['titulo']]['ISR'] = 0;
            
            
            
            
          
        }

        //print_r($memoryCalculoDataTitulo); die();
        
  

        $dataContent = $data->result_array();

       

        $totalMonto = 0;
        $aporte = 0;
        $ventaTitulo = 0;
        $comodin=0;

        $ingresos_total = 0;
        $costoSeguro = 0;
        $otrosEgresos = 0;

        $bandFirst = 0;


        $titulosA = array();
         
 


        
        $corte_inicio = "";
        $corte_final = "";

        $aporte_fecha = "";
        $aporte_monto = 0;
        $retiros = 0;



        // print_r($dataContent); die();

        

        foreach($dataContent as $row){


            if($row['MOVIMIENTO'] == "Aporte(+)"){

              //  $aporte_fecha = $row['FECHA'];

                foreach($arrayTitulosActual as $row2){

            
 
                    $var = $row2['titulo'];

                    
                        $memoryCalculoDataTitulo[$row2['titulo']]['aporte_fecha'] = $row['FECHA'];

                     //   $memoryCalculoDataTitulo[$row2['titulo']]['aporte_monto'] = $memoryCalculoDataTitulo[$row2['titulo']]['aporte_monto'] + floatNum($row['MONTO']);
                   
                       
                   
                  
                }
    
          
            }



         //   if(strtotime($row['FECHA'])>=strtotime($finicio) && strtotime($row['FECHA'])<= strtotime($ffinal) ){   

                if(  strtotime($row['FECHA'])<= strtotime($ffinal) ){   
                
                if($row['MOVIMIENTO'] == "Aporte(+)"){

                    //  $aporte_fecha = $row['FECHA'];
      
                      foreach($arrayTitulosActual as $row2){

                            $var = $row2['titulo'];
                            $memoryCalculoDataTitulo[$row2['titulo']]['aporte_monto'] = $memoryCalculoDataTitulo[$row2['titulo']]['aporte_monto'] + floatNum($row['MONTO']);
                         
                                
                        
                      }
          
                
                  }


                  //nuevo
                  if( stristr($row['MOVIMIENTO'],"Compra Unid (Switch)")!==FALSE){


                    //  $aporte_fecha = $row['FECHA'];
      
                      foreach($arrayTitulosActual as $row2){

                            $var = $row2['titulo'];
                            $memoryCalculoDataTitulo[$row2['titulo']]['aporte_monto'] = $memoryCalculoDataTitulo[$row2['titulo']]['aporte_monto'] + floatNum($row['MONTO']);
                         
                                
                        
                      }
          
                
                  }



                  if($row['MOVIMIENTO'] == "Aporte adicional (+)"){
                    foreach($arrayTitulosActual as $row2){
                        $memoryCalculoDataTitulo[$row2['titulo']]['aporte_adicional'] = $memoryCalculoDataTitulo[$row2['titulo']]['aporte_adicional'] + floatNum($row['MONTO']);
                       
                       
                        //   $aporte_monto+=floatval(quitCommas($row['MONTO']));
                    }
                } 

                if( stristr($row['MOVIMIENTO'],"Recate parcial (-)")!==FALSE || stristr($row['MOVIMIENTO'],"Rescate tota")!==FALSE){

                    foreach($arrayTitulosActual as $row2){
    
                        $var = $row2['titulo'];
    
                            $memoryCalculoDataTitulo[$row2['titulo']]['retiros'] =  $memoryCalculoDataTitulo[$row2['titulo']]['retiros'] + floatNum($row['MONTO']);
                           
                       
                    }
    
                
                }
    
               
            }
 
 
       

        

            if($row['MOVIMIENTO'] == "Compra de unidades (-)"||stristr($row['MOVIMIENTO'],"Compra Unid (Switch)")!==FALSE){
               
                
                if($row['SEG_SZ_USA_EQ']!="") {
                    $titulosA[] = "SEG_SZ_USA_EQ";
                }

                if($row['SEG_SZ_DEUDA_OP']!="") {
                    $titulosA[] = "SEG_SZ_DEUDA_OP";
                }

                if($row['SEG_SZ_STERUSD']!="") {
                    $titulosA[] = "SEG_SZ_STERUSD";
                }

                if($row['SEG_SZ_EURO_EQ']!="") {
                    $titulosA[] = "SEG_SZ_EURO_EQ";
                }

                if($row['SEG_SZ_STERGOB']!="") {
                    $titulosA[] = "SEG_SZ_STERGOB";
                }

                if($row['SEG_SZ_STEREAL']!="") {
                    $titulosA[] = "SEG_SZ_STEREAL";
                }
 


                if($row['SEG_SZ_FONSER']!=""){
                    $titulosA[] = "SEG_SZ_FONSER";
                } 
                
                foreach($arrayTitulosActual as $row2){

            
 
                    $var = $row2['titulo'];

                    if($row[$var]!="") {
                        $memoryCalculoDataTitulo[$row2['titulo']]['aporte'] = $memoryCalculoDataTitulo[$row2['titulo']]['aporte'] + floatNum($row[$var]);
                       
                    }
                  
                }
    


                $aporte+=floatval($row['SEG_SZ_FONSER']);
            }else{
                //$memoryCalculoDataTitulo[$row['titulo']]
                $ventaTitulo+=floatval($row['SEG_SZ_FONSER']);

                      
                foreach($arrayTitulosActual as $row2){

            
 
                    $var = $row2['titulo'];

                   // if(strtotime($row['FECHA'])>=strtotime($finicio) && strtotime($row['FECHA'])<= strtotime($ffinal) ){    
                    if( strtotime($row['FECHA'])<= strtotime($ffinal) ){    
                 
                    //if($row['FECHA']>=$finicio && $row['FECHA']<= $ffinal ){
              

                        if($row[$var]!="") { 
                            $memoryCalculoDataTitulo[$row2['titulo']]['venta_titulo']=$memoryCalculoDataTitulo[$row2['titulo']]['venta_titulo'] + floatNum($row[$var]); 
                        }
                    }
                    
                }

            }

           
       //     

       //Compra de unidades
           // if($row['MOVIMIENTO'] == "Compra de unidades (-)"){
               //nuevo
            if(stristr($row['MOVIMIENTO'],"Compra de unidades (-)")!==FALSE||stristr($row['MOVIMIENTO'],"Compra Unid (Switch)")!==FALSE){

           
                foreach($arrayTitulosActual as $row2){


                    $var = $row2['titulo'];

                    if($row[$var]!=""){
                    $memoryCalculoDataTitulo[$row2['titulo']]['comodin'] = $memoryCalculoDataTitulo[$row2['titulo']]['comodin'] + floatNum($row[$var]);

                   // echo $row2['titulo'] .":<b>". floatNum($row[$var]) . "</b>  :  " . $memoryCalculoDataTitulo[$row2['titulo']]['comodin'] . "<br>";

                }

                }
               // if($row['FECHA']>=$finicio && $row['FECHA']<= $ffinal ){
                if(strtotime($row['FECHA'])>=strtotime($finicio) && strtotime($row['FECHA'])<= strtotime($ffinal) ){    
                 
                    foreach($arrayTitulosActual as $row2){
    
                        $var = $row2['titulo'];
    
                            if($row[$var]!="") { 
                               
                                $memoryCalculoDataTitulo[$row2['titulo']]['SAR_ingresos'] = $memoryCalculoDataTitulo[$row2['titulo']]['SAR_ingresos'] + floatNum($row['MONTO']);
                                $memoryCalculoDataTitulo[$row2['titulo']]['TAR_ingresos'] = $memoryCalculoDataTitulo[$row2['titulo']]['TAR_ingresos'] + floatNum($row[$var]);
    
                            }
                    
                    }

                }


            


                
                
            }else{
               // $comodin-=floatval($row['SEG_SZ_FONSER']);

            

               foreach($arrayTitulosActual as $row2){
    
                        $var = $row2['titulo'];
 
                            if($row[$var]!="") { 
                                $memoryCalculoDataTitulo[$row2['titulo']]['comodin'] = $memoryCalculoDataTitulo[$row2['titulo']]['comodin'] - floatNum($row[$var]);
                             //   echo "<font color='red'>" . $row2['titulo'] ."  :   <b>". floatNum($row[$var]) . "</b>:" . $memoryCalculoDataTitulo[$row2['titulo']]['comodin'] . "</font><br>";

                            }
                    
                    }

            

              
                    if(strtotime($row['FECHA'])>=strtotime($finicio) && strtotime($row['FECHA'])<= strtotime($ffinal) ){    
                 
                    if( stristr($row['MOVIMIENTO'],"Compra Unid (Switch)")!==FALSE || stristr($row['MOVIMIENTO'],"Aporte")!==FALSE){


                        $ingresos_total +=floatNum($row['MONTO']);
                    }


                    

                    if( stristr($row['MOVIMIENTO'],"Monto Impuesto ISR")!==FALSE){
                       // $memoryCalculoDataTitulo[$row['titulo']]['ISR'] = 0;

                        $memoryCalculoDataTitulo[$row2['titulo']]['ISR'] = $memoryCalculoDataTitulo[$row2['titulo']]['ISR'] + floatNum($row['MONTO']);
                       

                    }

                       

                   


                    //ingresos  ==>  Compra Unid (Switch) (-) y aporte
                 
 

                    //select ROUND(SUM(case when CARGO like '%FEE%' OR MOVIMIENTO like '%IVA%'  OR MOVIMIENTO like '%ISR%' OR MOVIMIENTO like '%Rescate%' then MONTO  end),2) as OtrosEgresos FROM tbl_calculos  where POLIZA=12890 and FECHA>'2019-09-30' AND FECHA<'2019-10-31'

                  //  if(  stristr($row['MOVIMIENTO'],"iva")!==FALSE || stristr($row['CARGO'],"Rescate")!==FALSE || stristr($row['CARGO'],"ISR")!==FALSE || stristr($row['MOVIMIENTO'],"Recate")!==FALSE || stristr($row['MOVIMIENTO'],"Rescate")!==FALSE || stristr($row['MOVIMIENTO'],"Venta Unid (Switch")!==FALSE){
                    if( stristr($row['CARGO'],"Fee")!==FALSE ||  stristr($row['MOVIMIENTO'],"iva")!==FALSE || stristr($row['CARGO'],"Rescate")!==FALSE || stristr($row['CARGO'],"ISR")!==FALSE || stristr($row['MOVIMIENTO'],"Recate")!==FALSE || stristr($row['MOVIMIENTO'],"Rescate")!==FALSE || stristr($row['MOVIMIENTO'],"Venta Unid (Switch")!==FALSE){
                 
                         $otrosEgresos+=floatval($row['MONTO']);

                              
                            foreach($arrayTitulosActual as $row2){
            
                                $var = $row2['titulo'];


        
                                    if($row[$var]!="") { 

                                        $memoryCalculoDataTitulo[$row2['titulo']]['SAR_otros_egresos'] = $memoryCalculoDataTitulo[$row2['titulo']]['SAR_otros_egresos'] + floatNum($row['MONTO']);
                       
                                        $memoryCalculoDataTitulo[$row2['titulo']]['TAR_otros_egresos'] = $memoryCalculoDataTitulo[$row2['titulo']]['TAR_otros_egresos'] + floatNum($row[$var]);

                                    }
                            
                            }
        
                        }



                    if($row['MOVIMIENTO'] == "Costo cobertura (-)"){
                        if($row['CARGO']=="Costo cobertura Fallecimiento" || $row['CARGO']=="Costo Cobertura Adicional BAF" ){
                            $costoSeguro+=floatNum($row['MONTO']);
                        }
 
                    }

                   if($row['MOVIMIENTO'] == "Venta de unidades (+)"){
                   // if(stristr($row['MOVIMIENTO'],"Venta de unidades")!==FALSE){
                       
                        if( stristr($row['CARGO'],"Costo cobertura")!==FALSE || stristr($row['CARGO'],"Derecho de poliza")!==FALSE){

                       // if($row['CARGO']=="Costo cobertura Fallecimiento" || $row['CARGO']=="Costo Cobertura Adicional BAF" || $row['CARGO']=="Derecho de poliza" ){

                            foreach($arrayTitulosActual as $row2){
        
                                $var = $row2['titulo'];

                                //echo "$var:<b>".floatNum($row[$var]) . "</b><br>";
                                    
        
                                    if($row[$var]!="") { 
                                        $memoryCalculoDataTitulo[$row2['titulo']]['TAR_costo_seguro'] = $memoryCalculoDataTitulo[$row2['titulo']]['TAR_costo_seguro'] + floatNum($row[$var]);

                                        $memoryCalculoDataTitulo[$row2['titulo']]['SAR_costo_seguro'] = $memoryCalculoDataTitulo[$row2['titulo']]['SAR_costo_seguro'] + floatNum($row['MONTO']);
                                    }
                            
                            }
                        }
 
 
                    }

                 





                    if($row['MOVIMIENTO'] == "Gasto de gestión (-)"){
                        if($row['CARGO']== "Derecho de Póliza" ){
                            $costoSeguro+=floatNum($row['MONTO']);
                        }
 
                    }
                }


            }

            
          
            if(strtotime($row['FECHA'])>=strtotime($finicio) && strtotime($row['FECHA'])<= strtotime($ffinal) ){    
                 
           // if($row['FECHA']>=$finicio && $row['FECHA']<= $ffinal ){


                       
            


                foreach($arrayTitulosActual as $row2){


                    if($memoryCalculoDataTitulo[$row2['titulo']]['bandFirst'] == 0){

                        $memoryCalculoDataTitulo[$row2['titulo']]['corte_inicio'] = $memoryCalculoDataTitulo[$row2['titulo']]['comodin']; 
                        $corte_inicio =   $memoryCalculoDataTitulo[$row2['titulo']]['comodin']; //$comodin;
                        //$bandFirst = 1;
                        $memoryCalculoDataTitulo[$row2['titulo']]['bandFirst'] = 1;
                    }else{
                        $corte_final = $memoryCalculoDataTitulo[$row2['titulo']]['comodin']; //$comodin;
                        $memoryCalculoDataTitulo[$row2['titulo']]['corte_final'] = $memoryCalculoDataTitulo[$row2['titulo']]['aporte'] - $memoryCalculoDataTitulo[$row2['titulo']]['venta_titulo']  ;//$memoryCalculoDataTitulo[$row2['titulo']]['comodin'];
                    } 

                }
                
            }
            $totalMonto+=floatNum($row['MONTO']);
        }


        foreach($arrayTitulosActual as $row2){
            $memoryCalculoDataTitulo[$row2['titulo']]['titulo_cierre'] = floatNum( $memoryCalculoDataTitulo[$row2['titulo']]['aporte_monto']+ $memoryCalculoDataTitulo[$row2['titulo']]['aporte_adicional'])-$memoryCalculoDataTitulo[$row2['titulo']]['venta_titulo'];

        }

         
   /* print_r($memoryCalculoDataTitulo);   

         

        echo "totalMonto de $poliza:" . $totalMonto;
        echo "aporte inicial de $poliza es:" . $aporte;
        echo "gasto de Venta $poliza es:" . $ventaTitulo;
        $resta = $aporte - $ventaTitulo;
        echo "lo que resta es:" . $resta;
        echo "<br>CorteInicial:" . $corte_inicio;
        echo "<br>CorteFinal:" . $corte_final . "<br>";  die();   
 */

       // echo "titulos:" . print_r($titulosA); die();

      //  echo "titulo:" . $titulosA[0];


        $data = $this->Extract_data_model->getListTitles_Poliza($poliza);

        $data_TitulosPoliza = $data->result_array();

        $qtyPolizas =  count($data_TitulosPoliza);
        
      // echo "cantidad Titulos:" . $qtyPolizas;
        //print_r($data_TitulosPoiza);

   
        //die();

       // echo "ingresos:" .  $ingresos_total; die();
      


        //$contentData = array();
        
        //$contentData = array("titulo"=>"SEG_SZ_FONSER", "corte_inicio" => $corte_inicio, "corte_final" => $corte_final, "porcentaje_titulo" => $porcentajeTitulo, "costo_seguro" => $costoSeguro, "otros_egresos" => $otrosEgresos, "ingresos"=>$ingresos_total, "aporte_fecha" => $aporte_fecha, "aporte_monto" => $aporte_monto, "retiros" => $retiros);

       // print_r($memoryCalculoDataTitulo); die();
        return($memoryCalculoDataTitulo);




     }



     function getListFondos($poliza,$ffinal){


        
        $dataListHeaderTitulos = $this->Extract_data_model->getHeaders_fondos();

 
//select sum(SEG_SZ_USA_EQ) as SEG_SZ_USA_EQ, sum(SEG_SZ_DEUDA_OP) as SEG_SZ_DEUDA_OP, sum(SEG_SZ_STER_OP) as SEG_SZ_STER_OP, sum(SEG_SZ_EURO_EQ) as SEG_SZ_EURO_EQ, sum(SEG_SZ_STERGOB) as SEG_SZ_STERGOB, sum(SEG_SZ_STEREAL) as SEG_SZ_STEREAL, sum(SEG_SZ_FONSER) as SEG_SZ_FONSER, sum(SEG_SZ_STERDOW) as SEG_SZ_STERDOW  from tbl_calculos where POLIZA='12027' and MOVIMIENTO='Venta de unidades (+)'
//plan es q primero se obtiene de fondos,
//despues obtenemos otro refuerzo p sumar al array, y alli se agrega las faltantes.
//el plan es no perder los %, y la base original

        $data = $this->Extract_data_model->getListTitulos_fromPoliza($poliza);
        $dataListTitulos = $data->result_array();
        $dataListTitulos = $dataListTitulos[0];

        


        $data = $this->Extract_data_model->getTitlesBy105form($poliza,$ffinal);
        $data_refListTitulos = $data->result_array()[0];

      //  print_r($data_refListTitulos); 


        /*
        foreach($data_refListTitulos as $row){
            //if($)
            echo $row . "<br>";
        }*/

        $titles_tbl_calculo = array();
        $dif_titulos = array();

     //   echo "SEG_SZ_DEUDA_OP:".$data_refListTitulos['SEG_SZ_DEUDA_OP'];

         for($i=1;$i<(count($dataListHeaderTitulos)-1);$i++){

           // $fieldTitle = $dataListHeaderTitulos[$i];
           $fieldTitle = $dataListHeaderTitulos[$i];

           if($fieldTitle!="SEG_SZ_FONSER2"){
               $row_title = $data_refListTitulos[$fieldTitle];

               if($row_title!="0"){
                   //$titles_tbl_calculo =array($fieldTitle=>$row_title);
                   array_push($titles_tbl_calculo, array($fieldTitle=>$row_title));
               }

            //echo $fieldTitle.":". $data_refListTitulos[$fieldTitle]. "<br>";
                 }

            //echo "$fieldTitle:".$data_refListTitulos[$data_refListTitulos]."<br>";
          
          }

         //  print_r($titles_tbl_calculo); die();

          foreach($titles_tbl_calculo as  $title){

                foreach($title as $row => $campo){
                  //   echo $row."=>".$campo."<br>";
                }
              //print_r($title);
             // echo $title[0]."=>".$title[1];
          }



       // die();

  
  /*      $dif_titulos = array_diff($dataListTitulos, $titles_tbl_calculo);

        print_r($dataListTitulos); die();
*/
        
        
        $dataArray = array();
        $dataArray2 = array();
        $fieldTitle = "";

      for($i=1;$i<(count($dataListHeaderTitulos)-1);$i++){       
         
        $fieldTitle = $dataListHeaderTitulos[$i];
  
         if($dataListTitulos[$fieldTitle]!==""){
             
              $dataArray[] = array('titulo' => $fieldTitle, 'porcentaje' => $dataListTitulos[$fieldTitle]);
           
          } 
      }
       

      //print_r($dataArray); die();

      foreach($titles_tbl_calculo as  $title){
        //  echo "titulo:" . $title[0][0] . "<br>";


        //print_r($title);
        
        foreach($title as $row => $campo){

            for($i=0;$i<count($dataArray);$i++){

          
                if($row!=$dataArray[$i]['titulo']){
                    $dataArray[] = array('titulo' => $row, 'porcentaje' => 0);
                    
                }
            }
          
        }
      
  }
  /*  $result = array_unique($dataArray);
     print_r($result);*/

     $result = unique_multidim_array($dataArray,'titulo');

   
 

       return($result);

     }



     function getPrecioTitulo($fecha_inicio="2019-09-30", $fecha_final="2019-10-31", $arrayTitulosActual){

        

        $data = $this->Extract_data_model->getList_precio_titulos();

        $dataTitulos = $data->result_array();
        
        
        $arrayTitulo = array();
     

        foreach($arrayTitulosActual as $row){
            $arrayTitulo[$row['titulo']] = array();

            $arrayTitulo[$row['titulo']] = array("porcentaje"=>$row['porcentaje']);
          
        }

       // print_r($arrayTitulosActual); die();
        //print_r($arrayTitulo); die();


      
       //0.0026099681854248
       $titulo_inicio = "";

       $inicio  = "";
       $titulo_final = "";

        foreach($dataTitulos as $row){
 
            if($row['FECHA'] == $fecha_inicio){
        
        
        
                foreach($arrayTitulosActual as $row2){
       
                    $var = $row2['titulo'];
    
                    $arrayTitulo[$row2['titulo']]['titulo_inicio'] = $row[$var];
              
                }

            }

        }

     


        //0.0026099681854248
        foreach($dataTitulos as $row){
            if($row['FECHA'] == $fecha_final){

                foreach($arrayTitulosActual as $row2){

            
 
                $var = $row2['titulo'];
 
                $arrayTitulo[$row2['titulo']]['titulo_final'] = $row[$var];
              
                }




            }

        }

      
    

 
       // $efectiva = (($titulo_final-$titulo_inicio)/$titulo_inicio)*100;

        foreach($arrayTitulosActual as $row2){
            $tasa = "FIJA";
           
            $arrayTitulo[$row2['titulo']]['efectiva'] =  (($arrayTitulo[$row2['titulo']]['titulo_final']-$arrayTitulo[$row2['titulo']]['titulo_inicio'])/$arrayTitulo[$row2['titulo']]['titulo_inicio'])*100;
            
            $var = $row2['titulo'];

            if($var == "SEG_SZ_STER_OP" || $var == "SEG_SZ_STERUSD" || $var == "SEG_SZ_EURO_EQ" || $var == "SEG_SZ_USA_EQ"){
                $tasa = "VARIABLE";
                     
            }
            $arrayTitulo[$row2['titulo']]['tasa'] = $tasa;
            }


   
    
/*

        $dif_days = dateDifference($fecha_inicio,$fecha_final);
    

        $anual =  round(($efectiva/$dif_days)*360,2);*/

        $dif_days = 0;
        foreach($arrayTitulosActual as $row2){

            $dif_days = dateDifference($fecha_inicio,$fecha_final);

           
            $arrayTitulo[$row2['titulo']]['anual'] =  round(($arrayTitulo[$row2['titulo']]['efectiva']/$dif_days)*360,2);
          
        }

      //  print_r($arrayTitulo); die();

        return($arrayTitulo);

        /*
           print_r($arrayTitulo); die();

      


        $dataContent = array();

        $dataContent = array(
            "nombre_titulo" => "SEG-SZ-FONSER1",
            "titulo_inicio" => $titulo_inicio,
            "titulo_final" => $titulo_final,
            "efectiva" => $efectiva, 
            "anual" => $anual

        );

        */
 
        
       // print_r($dataContent);


        
       //return($dataContent);


      

     }














     function sendMail()
{
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'smtp.gmail.com',
        'smtp_port' => '587',
        'smtp_crypto' => 'tls',
        'smtp_user' => 'constantinokv@gmail.com',
        'smtp_pass' => 'santi2012####',
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
        );

        $message = '';
        $this->load->library('email', $config);
      $this->email->set_newline("\r\n");
      $this->email->from('constantinokv@gmail.com'); // change it to yours
      $this->email->to('constantinokv@hotmail.com');// change it to yours
      $this->email->subject('Resume from JobsBuddy for your Job posting');
      $this->email->message($message);
      if($this->email->send())
     {
      echo 'Email sent.';
     }
     else
    {
     show_error($this->email->print_debugger());
    }

}


  

}