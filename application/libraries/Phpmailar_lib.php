<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
//require_once APPPATH."/third_party/PHPExcel.php"; 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
 
class PHPMailer_Lib  { 
    public function __construct() { 
        //parent::__construct(); 
        log_message('Debug', 'PHPMailer class is loaded.');
    }
    
    
    public function load(){
        require_once APPPATH.'third_party/PHPMailer/Exception.php';
        require_once APPPATH.'third_party/PHPMailer/PHPMailer.php';
        require_once APPPATH.'third_party/PHPMailer/SMTP.php';

        $mail = new PHPMailer;
        return $mail;
    }
}