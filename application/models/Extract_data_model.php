<?php
class Extract_data_model extends CI_Model
{
	function select_poliza($noPoliza)
	{
		//$this->db->order_by('noPoliza', 'DESC');
		$this->db->where('noPoliza', $noPoliza);
		$query = $this->db->get('tbl_masiva_edocta');
		return $query;
	}


	function select_poliza_view($noPoliza)
	{
		//$this->db->order_by('noPoliza', 'DESC');
		$this->db->where('noPoliza', $noPoliza);
		$query = $this->db->get('estadoCta_view ');
		return $query;
	}


	function getDataPoliza($noPoliza){

 
		$this->db->where('POLIZA', $noPoliza);
		$query = $this->db->get('tbl_calculos ');
		return $query;

	}

	function getListPolizasUniversal(){
		//$this->db->where('POLIZA', $noPoliza);
		$query = $this->db->get('tbl_universo_polizas ');
		//$query = $this->db->get('unit_polizas ');
		return $query;

	}


	function getDataPolizaBetween($poliza,$ffinicio, $ffinal){

		

		$SQL = "select * from tbl_calculos where POLIZA='$poliza' and FECHA>'$ffinicio' and FECHA<='$ffinal'";

		$query = $this->db->query($SQL);
		return($query);

	}


	function getList_precio_titulos(){

 
		//$this->db->where('POLIZA', $noPoliza);
		$query = $this->db->get('tbl_precios_titulos ');
		return $query;

	}

	function getListTitles_Poliza($noPoliza){
		 
		$this->db->select('CARGO');
		$this->db->distinct();
		$this->db->where('POLIZA', $noPoliza);
		$this->db->like('CARGO', 'FEE');
		$query = $this->db->get('tbl_calculos ');
		return $query;


	}


	function getData_Ejecutivo($noPoliza){
		//-- Datos Barquero
//select * from int4310320191230 where CORREO=(select mail_ejec  from tbl_mail_anton where NPOLICY='12890' order by DEFFECDATE desc  limit 1)

$SQL = "select * from int4310320191230 where CORREO=(select mail_ejec  from tbl_mail_anton where NPOLICY='$noPoliza' order by DEFFECDATE desc  limit 1)";

		$query = $this->db->query($SQL);
		return($query);




	}


	function getDate_Emision($noPoliza){

	//	select DISSUEDAT from tbl_mail_anton

		$this->db->select('DISSUEDAT');

		$this->db->where('NPOLICY', $noPoliza);
	 
		$query = $this->db->get('tbl_mail_anton ');

		return($query);
	}

	function getTitlesBy105form($noPoliza,$ffinal){		
		//$sql = "select sum(SEG_SZ_USA_EQ) as SEG_SZ_USA_EQ, sum(SEG_SZ_STERUSD) as SEG_SZ_STERUSD, sum(SEG_SZ_DEUDA_OP) as SEG_SZ_DEUDA_OP, sum(SEG_SZ_STER_OP) as SEG_SZ_STER_OP, sum(SEG_SZ_EURO_EQ) as SEG_SZ_EURO_EQ, sum(SEG_SZ_STERGOB) as SEG_SZ_STERGOB, sum(SEG_SZ_STEREAL) as SEG_SZ_STEREAL, sum(SEG_SZ_FONSER) as SEG_SZ_FONSER, sum(SEG_SZ_STERDOW) as SEG_SZ_STERDOW  from tbl_calculos where POLIZA='$noPoliza' and MOVIMIENTO='Venta de unidades (+)'";
		//$sql = "select IFNULL(sum(SEG_SZ_USA_EQ),0) as SEG_SZ_USA_EQ, IFNULL(sum(SEG_SZ_STERUSD),0) as SEG_SZ_STERUSD, IFNULL(sum(SEG_SZ_DEUDA_OP),0) as SEG_SZ_DEUDA_OP, IFNULL(sum(SEG_SZ_STER_OP),0) as SEG_SZ_STER_OP, IFNULL(sum(SEG_SZ_EURO_EQ),0) as SEG_SZ_EURO_EQ, IFNULL(sum(SEG_SZ_STERGOB),0) as SEG_SZ_STERGOB, IFNULL(sum(SEG_SZ_STEREAL),0) as SEG_SZ_STEREAL, IFNULL(sum(SEG_SZ_FONSER),0) as SEG_SZ_FONSER, IFNULL(sum(SEG_SZ_STERDOW),0) as SEG_SZ_STERDOW  from tbl_calculos where POLIZA='$noPoliza' and MOVIMIENTO like '%Compra Unid (Switch)%' and FECHA<'$ffinal'";

		$sql = "select IFNULL(sum(SEG_SZ_USA_EQ),0) as SEG_SZ_USA_EQ, IFNULL(sum(SEG_SZ_STERUSD),0) as SEG_SZ_STERUSD, IFNULL(sum(SEG_SZ_DEUDA_OP),0) as SEG_SZ_DEUDA_OP, IFNULL(sum(SEG_SZ_STER_OP),0) as SEG_SZ_STER_OP, IFNULL(sum(SEG_SZ_EURO_EQ),0) as SEG_SZ_EURO_EQ, IFNULL(sum(SEG_SZ_STERGOB),0) as SEG_SZ_STERGOB, IFNULL(sum(SEG_SZ_STEREAL),0) as SEG_SZ_STEREAL, IFNULL(sum(SEG_SZ_FONSER),0) as SEG_SZ_FONSER, IFNULL(sum(SEG_SZ_STERDOW),0) as SEG_SZ_STERDOW  from tbl_calculos where POLIZA='$noPoliza' and (MOVIMIENTO like '%Compra Unid (Switch)%' or MOVIMIENTO like '%Compra de unidades (-)%' or MOVIMIENTO like '%Venta de unidades (+)%' and FECHA<'$ffinal')";		
		
		
		$query = $this->db->query($sql);
		return($query);

	}


	function getListTitulos_fromPoliza($noPoliza){ 
			 
			$this->db->where('POLIZA', $noPoliza);
		 
			$query = $this->db->get('tbl_fondos_101 ');
	
			return($query);
		}

	function getHeaders_fondos(){
		$query = $this->db->list_fields('tbl_fondos_101');

		return($query);
	}
	
	 
	function validateContent($table, $campo, $poliza, $condicion = ""){ 
		
		
		$SQL = "select * from $table where $campo='$poliza' $condicion";

		$query = $this->db->query($SQL);
		 
		return ($query->num_rows() > 0) ? FALSE : TRUE;

	}
 
	 
}



