<?php


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
        //Page header
        public function Header() {
            // Logo
            $image_file = K_PATH_IMAGES.'zurich-logo.jpg';
            $this->Image($image_file, 10, 3, 80, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            // Set font
    
            $this->SetY(13);
            $this->SetFont('helvetica', 'B', 12);
           
            // Producto 
            $this->Cell(-20, 25, 'UNIT LINKED SANTANDER', 0, false, 'R', 0, '', 0, false, 'M', 'M');
            $this->SetY(18);
            $this->SetFont('helvetica', '', 12);
            // Subtitle
             $this->Cell(0, 0, 'ESTADO DE CUENTA', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        }
 

    // Page footer
    public function Footer() {

        $this->SetY(-25);
        // Set font
        $this->SetFont('helvetica', ' ', 8);
        // Page number
        $this->Cell(0, 10, 'Av. Juan Salvador Agraz #73, pisos 3 y 4 Col. Santa Fe Cuajimalpa, Del.Cuajimalpa de Morelos, CP. 05348, CDMX, Mexico, Tel 51', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        $this->SetY(-22);
        // Set font
        $this->SetFont('helvetica', ' ', 8);
        // Page number
        $this->Cell(0, 10, '69 43 00 en la Cd. de Mexico y area meropolitana o lada sin costo 01 800 501 0000 del interior de la Republica.', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('UNIT LINKED SANTANDER');
$pdf->SetSubject('ESTADO DE CUENTA');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData('', 120, 'UNIT LINKED SANTANDER', 'ESTADO DE CUENTA');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// -------------------------------------------------------------------

// add a page
$pdf->AddPage();
// create some HTML content
$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';
 $pdf->SetFont('Helvetica', ' ', 10);

$estilo = '
    <style>
        tr.border_bottom td {
            border-bottom:1pt solid black;
        }


        tr.border_top td {
            border-top:1pt solid black;
          }

          tr.border_top_light td {
            border-top:0.5pt solid black;
          }

          td.border_top_light_td {
            border-top:0.5pt solid black;
          }
          td.border_light_td {
            border:0.5pt solid black;
          }

          td.border_top_light {
            border:0.5pt solid black;
           
          
          }

     


        tr.border_Subbottom td {
            border-bottom:0.5pt solid black;
          }


          span.smalltext {
            font-size: .8em; /* .8em x 10px = 8px */
          }

          td.smalltext {
            font-size: .8em; /* .8em x 10px = 8px */
          }

          
          span.normaltext {
            font-size: .12em; /* .8em x 10px = 8px */
          }

          td.normaltext {
            font-size: .12em; /* .8em x 10px = 8px */
          }

          td.negrita {
            font-weight: bold;
          }

 
    </style> ';

$titulo = '

    <table>
        <tr class="border_top"><td></td></tr>
  
        <tr>
        <td><h4>Zurich Santander Seguros México S.A.</h4></td></tr>
        <tr><td></td></tr>
    </table>';


$t_datosContratante = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="2"><b>DATOS DEL CONTRATANTE</b></td>
        <td colspan="2" align="right">PERIODO DEL: <b>01/10/2019</b> AL <b>31/10/2019</b></td>
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>CONTRATANTE:</b></td>
                    <td width="220">ANTONIO</td>
                    <td width="150"><b>NUMERO DE PÓLIZA:</b></td>
                    <td>1 102 00000012020</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>ASEGURADO:</b></td>
                    <td width="220">ANTONIO</td>
                    <td width="150"><b>PRODUCTO:</b></td>
                    <td>UNIT LINKED SATANDER</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>DOMICILIO:</b></td>
                    <td width="220">SONORA</td>
                    <td width="150"><b>MONEDA:</b></td>
                    <td>PESOS</td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE EMISIÓN:</b></td>
                    <td>24/05/2019</td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE VENCIMIENTO:</b></td>
                    <td>19/05/2019</td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE CORTE:</b></td>
                    <td>31/10/2019</td>
                </tr>
            </table>

        </td>
    </tr>
     
     
   
</table>';


$t_datosBanquero = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DATOS DEL BANQUERO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>EJECUTIVO:</b></td>
                    <td width="150">JOSE DANIEL</td>
                    <td width="160"><b>TELÉFONO:</b></td>
                    <td width="180"></td>
                 
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>CÓDIGO:</b></td>
                    <td width="150"> </td>
                    <td width="160"><b>CORREO ELECTRÓNICO:</b></td>
                    <td width="180">constantinokv@santander.com</td>
                   
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>SUCURSAL:</b></td>
                    <td width="150">CONS. AREA DE NEGOCIO</td>
                    <td width="150"> </td>
                    <td> </td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
     
     
   
</table>';

$t_coberturasAmparadas = '
<table border="0" width="770" bgcolor="">
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>COBERTURAS AMPARADAS</b></td>
        
        
    </tr>
    <tr>
       <td colspan="5" >
          <table border="0" width="100%">
                <tr>
                    <td> </td>
                    <td> </td>
                    <td align="center"><b>Suma Asegurada</b></td>
                </tr>
            </table>
        </td>
    </tr>
 

    <tr>
    <td colspan="5" >
        <table  width="740" border="0" >
                <tr>
                    <td width="3"></td>
                    <td></td>
                    <td width="305"> </td>
                    <td width="100" align="right" class="border_top_light_td"><b>Alcanzada</b></td>
                    <td width="100" align="right" class="border_top_light_td" ><b>Contratada</b></td>
                </tr>
            </table>
    </td>
</tr>

    <tr>
        <td colspan="5" >
            <table    width="740" border="0" >
                    <tr class="border_top_light"> 
                        <td width="3"></td>
                        <td><b>Cobertura Basica:</b></td>
                        <td width="305">Fallecimiento</td>
                        <td width="100" align="right">10,000</td>
                        <td width="100" align="right">10,000</td>
                    </tr>
                </table>
        </td>
    </tr>


    
    <tr>
     <td colspan="5"></td>
    </tr>
        
 

 
     
     
   
</table>';

$t_resumenPeriodo = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>RESUMEN DE MOVIMIENTOS DEL PERÍODO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    
    <tr>
        <td width="80"></td>
        <td colspan="4">
            <table border="0" width="500">

                <tr class="border_Subbottom">
                        
                    <td width="250">Saldo al Inicio</td>
                    <td width="150" align="right">13,217,783.89</td>
            
                </tr>
                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(+) Ingreso </td>
                    <td width="150" align="right">0.00</td>
                
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Aportaciones </td>
                    <td width="150" align="right">0.00</td>
            
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Ingreso por Ajuste a la Reserva </td>
                    <td width="150" align="right">0.00</td>
        
                </tr>


                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(-) Egreso </td>
                    <td width="150" align="right">8,366.45</td>
                
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Costo del Seguro y Derecho de Póliza </td>
                    <td width="130" align="right">12.69</td>
        
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Cargos a la Póliza</td>
                    <td width="130" align="right">8,366.45</td>
    
                 </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Retiros</td>
                    <td width="130" align="right">0.00</td>
 
                </tr>


                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Impuesto Sobre la Renta</td>
                    <td width="130" align="right">0.00</td>
 
                </tr>
                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(+) Rendimientos </td>
                    <td width="150" align="right">84,092.68</td>
                
                </tr>


                <tr class="border_Subbottom">
                            
                    <td width="250">Saldo al Cierre**</td>
                    <td width="150" align="right">13,393,497.43</td>
        
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                

                
            </table>

        </td>

    </tr>



    <tr>
   
    <td colspan="4">
        <table border="0" width="600">
          <tr>
                <td class="smalltext" >
                        ** La Aseguradora con base en el Artículo 93, Fracción XXI de LISR pagara el rendimiento sin retención de impuesto, siempre y   </td>
            </tr>
            <tr>

            <td class="smalltext" >
                        
                        
                        
                        cuando el asegurado cumpla con los siguiente: 
                        </td> 
            </tr>
            <tr>
                <td class="smalltext">
                            a) A la fecha de rescate tenga 60 años de edad o más.
                            <br>
                            b) Que hayan transcurrido al menos 5 años desde la fecha de contratación del seguro y el momento en que se pague el rescate.
                            <br>
                            c)Que el Asegurado sea el Contratante de la Póliza.
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            
            <tr>
                <td class="smalltext">
                        La Aseguradora emitirá la constancia de retención de impuestos por el 20% sobre interés real cuando el Asegurado rescata su póliza, 
                </td>
                
                </tr>
            <tr>
                <td class="smalltext">
                 y no cumpla con los requisitos anteriormente citados.
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>



            <tr>
                <td class="smalltext">
                       Sujeto a lo previsto en la normatividad vigente al momento del pago del rendimiento.
                </td>
            </tr>
        </table>

    </td>

</tr>


    

   

 
     
     
   
</table>';


$htmlCuerpo1 = $estilo . $titulo . $t_datosContratante . $t_datosBanquero . $t_coberturasAmparadas. $t_resumenPeriodo;

// output the HTML content
$pdf->writeHTML($htmlCuerpo1, true, false, true, false, '');





$t_datosBanquero = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DATOS DEL BANQUERO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>EJECUTIVO:</b></td>
                    <td width="150">JOSE DANIEL</td>
                    <td width="160"><b>CORREO ELECTRÓNICO:</b></td>
                    <td width="180">constantinokv@santander.com</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>CÓDIGO:</b></td>
                    <td width="150"> </td>
                    <td width="160"><b>TELÉFONO:</b></td>
                    <td width="180"></td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>SUCURSAL:</b></td>
                    <td width="150">CONS. AREA DE NEGOCIO</td>
                    <td width="150"> </td>
                    <td> </td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
     
     
   
</table>';





/////////////////////////////////////////////////////////////////////////////////////////////////  PAGE 2




$t_datosResumenRendimientosAlternativa = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>RESUMEN DE RENDIMIENTOS POR ALTERNATIVA</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="32"  > </td>
                    <td align="center" class="border_top_light" ><b>Alternativa de Rendimiento </b> </td>
                    <td align="center" class="border_top_light"><b>Distribucion de Portafolio</b></td>
                    <td class="border_top_light" >
                        <table   cellspacing=0 style="width: 100%; padding:0px; margin: 0px;">
                       
                            <tr>
                                <td colspan="2" align="center"  ><b>Precio Titulo</b></td>
                            </tr>
                            <tr>
                                <td align="center"  ><b>Inicio</b></td><td align="center" ><b>Fin</b></td>
                            </tr>
                        </table>
                    </td>

                    <td align="center" class="border_top_light">
                        <b>Tasa de Rendimiento <sup>2</sup></b>
                    </td>
            
                </tr>
            </table>

        </td>
    
    </tr>

    
   
<tr>
<td colspan="5">
    <table border="0" width="740">
        <tr>
            <td width="32"> </td>
            <td align="center" >SEG-SZ-STERGOB</td>
            <td align="center">100.00%</td>
            <td>
                <table>
                
                    <tr>
                        <td align="center">2.333061</td><td align="center">2.347906</td>
                    </tr>
                </table>
            </td>

            <td align="center">
               7.64% <sup>(a)</sup>
                            </td>
    
        </tr>
        <tr>
        <td width="32"> </td>
        <td align="center" >SEG-SZ-STERGOB</td>
        <td align="center">100.00%</td>
        <td>
            <table>
            
                <tr>
                    <td align="center">2.333061</td><td align="center">2.347906</td>
                </tr>
            </table>
        </td>

        <td align="center">
           7.64% <sup>(a)</sup>
                        </td>

    </tr>
    </table>

</td>

</tr>
<tr>
<td colspan="5"> </td>
       
</tr>

<tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr>
                
                <td>
                    <table border="0">
                        <tr>
                            <td  class="smalltext" width="20">   
                                (1) 
                            </td>
                            <td class="smalltext" width="650" >
                                Porcentaje de distribucion de Saldo a la fecha de corte, corrrepsondiente a la proprocion de cada una de la alternativas de rendimiento.
                            
                            </td>


                        </tr>

                        <tr>
                            <td  class="smalltext" width="20">   
                                (2)  
                            </td>
                            <td class="smalltext"  width="600">
                                (a) Tasa efectiva anual en el periodo para Alternativas de renta fija. (b) Rendimiento directo para alternativas en renta variable.
                            
                            </td>

                        </tr>
                
                
                
                                
                    </table>
                
                </td>
        
            </tr>
        </table>

    </td>

</tr>

<tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
</table>';






$t_datosAlternativasRendimiento = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>TITULOS DE ALTERNATIVAS DE RENDIMIENTO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                     
                    <td align="center"  width="120"  class="border_top_light" ><b>Alternativa de Rendimiento </b> </td>
                    <td align="center"   width="120"  class="border_top_light"><b>% Distribucion de Ingresos</b></td>
                    <td align="center" class="border_top_light"><b>Inicio</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(+) Ingresos</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Costos del Seguro</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Otros Egresos</b></td>
                    <td align="center" class="border_top_light" width="100"><b>Titulos al Cierre</b></td>

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr class="">
                     
                    <td  width="120"  >SEG-SZ-STERGOB </td>
                    <td  align="right" >100.00%</td>
                    <td align="right"  >5,665,427.76</td>
                    <td    width="70" align="right">0.00</td>
                    <td    width="70" align="right">5.41</td>
                    <td    width="70" align="right">3,567.86</td>
                    <td    width="100" align="right">5,661,852.49</td>

                    
            
                </tr>
                <tr class="border_Subbottom">
                     
                <td  width="120"  >SEG-SZ-STERGOB </td>
                <td  align="right" >100.00%</td>
                <td align="right"  >5,665,427.76</td>
                <td    width="70" align="right">0.00</td>
                <td    width="70" align="right">5.41</td>
                <td    width="70" align="right">3,567.86</td>
                <td    width="100" align="right">5,661,852.49</td>

                
        
            </tr>
            </table>

        </td>
    
    </tr>

    <tr>
    <td colspan="5">
        <table border="0" width="740"  >
            <tr  >
                 
                   
            <td  width="120"  >  </td>
            <td  align="right" ><b>TOTAL TITULOS</b></td>
                
                <td align="right"  ><b>5,665,427.76</b></td>
                <td    width="70" align="right"><b>0.00</b></td>
                <td    width="70" align="right"><b>5.41</b></td>
                <td    width="70" align="right"><b>3,567.86</b></td>
                <td    width="100" align="right"><b>5,661,852.49</b></td>

                
        
            </tr>
        </table>

    </td>

</tr>

    
    

<tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
</table>';





$t_datosAlternativasRendimientoMXN = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>TITULOS DE ALTERNATIVAS DE RENDIMIENTO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                     
                    <td align="center"  width="120"  class="border_top_light" ><b>Alternativa de Rendimiento </b> </td>
                    <td align="center"   width="120"  class="border_top_light"><b>Saldo al Inicio</b></td>
                    <td align="center" class="border_top_light"><b>(+) Ingresos</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Costos del Seguro</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Otros Egresos</b></td>
                    <td align="center" class="border_top_light" width="100"><b>(+) Rendimientos</b></td>
                    <td align="center" class="border_top_light" width="100"><b>Saldos al Cierre</b></td>

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr class="">
                     
                    <td  width="120"  >SEG-SZ-STERGOB </td>
                    <td  align="right" >13,217,783.89</td>
                    <td align="right"  >0.00</td>
                    <td    width="70" align="right">12.69</td>
                    <td    width="70" align="right">8,366.45</td>
                    <td    width="100" align="right">84,092.68</td>
                    <td    width="100" align="right">13,293,497.43</td>

                    
            
                </tr>
                <tr class="">
                     
                <td  width="120"  >SEG-SZ-STERGOB </td>
                <td  align="right" >13,217,783.89</td>
                <td align="right"  >0.00</td>
                <td    width="70" align="right">12.69</td>
                <td    width="70" align="right">8,366.45</td>
                <td    width="100" align="right">84,092.68</td>
                <td    width="100" align="right">13,293,497.43</td>

                
        
            </tr>
            <tr class="">
                        
                <td  width="120"  >SEG-SZ-STERGOB </td>
                <td  align="right" >13,217,783.89</td>
                <td align="right"  >0.00</td>
                <td    width="70" align="right">12.69</td>
                <td    width="70" align="right">8,366.45</td>
                <td    width="100" align="right">84,092.68</td>
                <td    width="100" align="right">13,293,497.43</td>

                
        
            </tr>
            <tr class="">
                        
                <td  width="120"  >SEG-SZ-STERGOB </td>
                <td  align="right" >13,217,783.89</td>
                <td align="right"  >0.00</td>
                <td    width="70" align="right">12.69</td>
                <td    width="70" align="right">8,366.45</td>
                <td    width="100" align="right">84,092.68</td>
                <td    width="100" align="right">13,293,497.43</td>

            </tr>
            <tr class="">
                        
            <td  width="120"  >SEG-SZ-STERGOB </td>
            <td  align="right" >13,217,783.89</td>
            <td align="right"  >0.00</td>
            <td    width="70" align="right">12.69</td>
            <td    width="70" align="right">8,366.45</td>
            <td    width="100" align="right">84,092.68</td>
            <td    width="100" align="right">13,293,497.43</td>

        </tr>
        <tr class="border_Subbottom">
                        
        <td  width="120"  >SEG-SZ-STERGOB </td>
        <td  align="right" >13,217,783.89</td>
        <td align="right"  >0.00</td>
        <td    width="70" align="right">12.69</td>
        <td    width="70" align="right">8,366.45</td>
        <td    width="100" align="right">84,092.68</td>
        <td    width="100" align="right">13,293,497.43</td>

    </tr>
            </table>


        </td>
    
    </tr>

    <tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr  >
                 
                <td  width="120"  ><b>TOTAL SALDO</b></td>
                <td  align="right" ><b>13,217,783.89</b></td>
                <td align="right"  ><b>0.00</b></td>
                <td    width="70" align="right"><b>12.69</b></td>
                <td    width="70" align="right"><b>8,366.45</b></td>
                <td    width="100" align="right"><b>84,092.68</b></td>
                <td    width="100" align="right"><b>13,293,497.43</b></td>

                
        
            </tr>
        </table>

    </td>

</tr>
    
    

<tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>

    <tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr>
                
                <td>
                    <table border="0">
                        <tr>
                            <td  class="smalltext" width="20">   
                                (3) 
                            </td>
                            <td class="smalltext" width="650" >
                               Porcentaje actual de distribución de ingresos, el cual es utilizado para distribuir las aportaciones de prima recibidos en la póliza.
                            </td>


                        </tr>

                        <tr>
                            <td  class="smalltext" width="20">   
                                (4)  
                            </td>
                            <td class="smalltext"  width="600">
                               Cargos aplicados por concepto mortalidad, costo de administración y derecho de póliza, tal como se específica en las condiciones generales del producto.
                            </td>

                        </tr>
                

                        <tr>
                        <td  class="smalltext" width="20">   
                            (4)  
                        </td>
                        <td class="smalltext"  width="600">
                         Retiros efectuados a petición del Contratante, impuestos, costo de administración de cada alternativa de rendimiento y cargos generados por movimientos realizados por el cliente.                    </td>

                    </tr>
            
                
                
                                
                    </table>
                
                </td>
        
            </tr>
        </table>

    </td>

</tr>
 

 
</table>';



// add a page
$pdf->AddPage();







$htmlCuerpo2 = $estilo .  $t_datosResumenRendimientosAlternativa . $t_datosAlternativasRendimiento . $t_datosAlternativasRendimientoMXN;

$pdf->writeHTML($htmlCuerpo2, true, false, true, false, '');





// add a page
$pdf->AddPage();





$t_datosPresentarSolicitudDeAclaracionoServicio = '
<table border="0" width="770"  >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>PARA PRESENTAR UNA SOLICITUD DE ACLARACIÓN O SERVICIO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
    <td colspan="5">
            <table border="0" width="100%">
                <tr>
                    
                    <td>
                        <table border="0"   >
                            <tr>
                               
                                <td class="smallnormaltexttext" width="*" >
                                <b>UNIDAD ESPECIALIZADA PARA LA ATENCION DE USUARIOS (UEA)</b>
                                <br>
                                Av. Juan Salvador Agraz #73, piso 3, Col. Santa Fe Cuajimalpa, Del. Cuajimalpa de Morelos, C.P. 05348, Ciudad
                                <br> 
                                de México Correo Electrónico: ueaseguros@santander.com.mx
                                <br>
                                <br>
                                Teléfonos: 55 1037-3500 Ext. 13566, 13571 y 13515, con un horario de atención de lunes a jueves de 7:30 a 
                                <br>
                                17:00 horas y viernes de 7:30 a 14:00 horas.
                                <br>
                                <br>
                                <b>CONDUSEF</b>
                                <br>
                                Av. Insurgentes Sur #762, planta baja, Col. Del Valle, Del. Benito Juarez, C.P.03100, Ciudad de México.
                                <br>
                                Correo Electrónico: asesoria@condusef.gob.mx, Pagina Web: http://www.cundusef.gob.mx, Teléfonos: 01 800 999
                                <br>
                                8080 o 5340 0999 

                                </td>


                            </tr>
 
                
                    
                    
                                    
                        </table>
                    
                    </td>
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5"> </td>
       
    </tr>



</table>';



    $t_datosComisionesAplicadasPromedio = '

    <table border="0" width="770" >
        
        <tr class="border_bottom">
            <td width="40"> </td>
            <td colspan="4"><b>COMISIONES PROMEDIO APLICADAS DE SIEFORES Y FONDOS DE INVERSIÓN</b></td>
        </tr>


        <tr>
        <td colspan="5"> </td>
        </tr>
        <tr>
        <td colspan="5"> </td>
        </tr>
        
        
        <tr>

        
        <td  width="750" colspan=5 >



<table width="100%" border="0">

<tr>

  <td width="200"><table width="100%" border="0">

  <tr>
  <td colspan="2" class="smalltext" align="center"> Comisiones de las Siefores Básicas (Al cierre de Diciembre de 2017) <br>
    </td>
  </tr>

    <tr bgcolor="#333333">

      <td width="56%" class="smalltext"><div align="center" ><br><br><br><font color="White"><b> Afore</b></font></div></td>

      <td width="44%"><table width="100%" border="1">

        <tr>

          <td class="smalltext" height="60"><div align="center">  <br><br><font color="white"><b>Porcentaje</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext"><div align="center"><font color="white"><b>Anual</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext"><div align="center"><font color="white"><b>Sobre Saldo</b></font></div></td>

        </tr>

      </table></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Azteca</b></td>

      <td class="smalltext border_light_td" ><div align="right">1.100</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Banamex</b></td>

      <td class="smalltext border_light_td"><div align="right">0.990</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Coppel</b></td>

      <td class="smalltext border_light_td"><div align="right">1.100</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Inbursa</b></td>

      <td class="smalltext border_light_td"><div align="right">0.980</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Invercap</b></td>

      <td class="smalltext border_light_td"><div align="right">1.100</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Metlife</b></td>

      <td class="smalltext border_light_td"><div align="right">1.100</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>PensionISSSTE</b></td>

      <td class="smalltext border_light_td"><div align="right">0.860</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Principal</b></td>

      <td class="smalltext border_light_td"><div align="right">1.090</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Profuturo GNP</b></td>

      <td class="smalltext border_light_td"><div align="right">1.030</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>SURA</b></td>

      <td class="smalltext border_light_td"><div align="right">1.030</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>XXI Banorte</b></td>

      <td class="smalltext border_light_td"><div align="right">1.000</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Promedio</b></td>

      <td class="smalltext border_light_td"><div align="right">1.035</div></td>

    </tr>

  </table></td>

  <td width="33">&nbsp;</td>

  <td width="400"><table  width="100%" border="0">

    <tr>

      <td class="smalltext" align="center"> Comisiones de los Productos de Ahorro IV 5 <br>

        (Al Cierre de Diciembre de 2017)<br></td>

    </tr>

    <tr>

      <td class="smalltext"><table width="100%" border="1">

        <tr bgcolor="#333333">

          <td class="smalltext" width="60"><div align="center"><br><br><br><font color="white"><b>No.Fondo</b></font></div></td>

          <td class="smalltext" width="230"><div align="center"><br><br><br><br><font color="white"><b>NOM_FONDO</b></font></div></td>

          <td class="smalltext" width="50"><div align="center"><font color="white"><b>Porcentaje sobre el saldo antes de la renovación.</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext" align="center">1</td>

          <td class="smalltext">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext"  align="center">3</td>

          <td class="smalltext">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="right">1.100%</div></td>

        </tr>

        <tr>

          <td class="smalltext"  align="center">4</td>

          <td class="smalltext">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext"  align="center">6</td>

          <td class="smalltext">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="right">1.150%</div></td>

        </tr>

        <tr>

          <td class="smalltext" >&nbsp;</td>

          <td class="smalltext"  align="center"><b>Promedio</b></td>

          <td class="smalltext"><div align="right"></div></td>

        </tr>

        <tr>

          <td class="smalltext" align="center">2</td>

          <td class="smalltext">FONDO SANTANDER IV 2 - MIXTO</td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext" align="center">5</td>

          <td class="smalltext">FONDO SANTANDER IV 2 - MIXTO</td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext">&nbsp;</td>

          <td class="smalltext"  align="center"><b>Promedio</b></td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

      </table></td>

    </tr>

    <tr>

      <td class="smalltext" align="center"> <br><br>Comisiones de las Sociedades de Inversión<br>

        (Al cierre de Sepitembre de 2015)<br></td>

    </tr>

    <tr>

      <td class="smalltext"><table width="100%" border="0">

        <tr bgcolor="#333333">

          <td class="smalltext"  width="60"><div align="center"><font color="white"><br><b>S.INVERSION</b></font> </div></td>

          <td class="smalltext"  width="230"><div align="center"><font color="white"><br><b>EMISORA</b></font></div></td>

          <td class="smalltext"  width="60"><div align="center"  ><font color="white"><b>Porcentaje Anual Sobre Saldo</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">IXELQ BF2</td>

          <td class="smalltext border_light_td">FONDO DE BANORTEIXE (GUB/PRIVADOS)</td>

          <td class="smalltext border_light_td"><div align="right">1.500%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">BMER180 B2</td>

          <td class="smalltext border_light_td">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext border_light_td"><div align="right">1.939%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">BMER30 B2</td>

          <td class="smalltext border_light_td">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext border_light_td"><div align="right">1.939%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">BMER90 B2</td>

          <td class="smalltext border_light_td">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext border_light_td"><div align="right">1.939%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">&nbsp;</td>

          <td class="smalltext border_light_td"><div align="center"><b>Promedio</b></div></td>

          <td class="smalltext border_light_td"><div align="right">1.829%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">ACTIVCO B-2</td>

          <td class="smalltext border_light_td">FONDO DE ACTINVER (RENTA FIJA/VARIABLE)</td>

          <td class="smalltext border_light_td"><div align="right">1.068%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">DIVER-CP</td>

          <td class="smalltext border_light_td">FONDO DE BBVA BANCOMER (RENTA FIJA/VARIABLE)</td>

          <td class="smalltext border_light_td"><div align="right">1,989%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">SURV10 BF2</td>

          <td class="smalltext border_light_td">FONDO DE SURA(RENTA FIJA/VARIABLE)</td>

          <td class="smalltext border_light_td"><div align="right">1.700</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">&nbsp;</td>

          <td class="smalltext border_light_td"><div align="center"><b>Promedio</b></div></td>

          <td class="smalltext border_light_td"><div align="right">1.586%</div></td>

        </tr>

      </table></td>

    </tr>

    <tr>

      <td class="smalltext">&nbsp; </td>

    </tr>

    <tr>

      <td class="smalltext">&nbsp;</td>

    </tr>

  </table></td>

</tr>

  </table></td>

</tr>

</table>


          
        
        </td>
        
        </tr>
    
    

    
    </table>';


$htmlCuerpo3 = $estilo .  $t_datosPresentarSolicitudDeAclaracionoServicio . $t_datosComisionesAplicadasPromedio;

$pdf->writeHTML($htmlCuerpo3, true, false, true, false, '');




$pdf->AddPage();


//////////////////////////////////////////////////////////////////////////////////  Page 4





$t_datosAportacionesyRetirosHistorico = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>APORTACIONES Y RETIROS (HISTO*RICO)</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                     
                    <td align="center"  class="border_top_light" width="60" ><b>Año Póliza</b> </td>
                    <td align="center"  class="border_top_light" width="80" ><b>Fecha Desde</b></td>
                    <td align="center"  class="border_top_light" width="80" ><b>Fecha Hasta</b></td>
                    <td align="center" class="border_top_light" width="80"><b>Prima Pactada</b></td>
                    <td align="center" class="border_top_light" width="80"><b>Primas Pagadas</b></td>
                    <td align="center" class="border_top_light" width="120"><b>Prima Adicional Pagada</b></td>
                    <td align="center" class="border_top_light" width="120"><b>Retiros Efectuados</b></td>

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr class=" ">
                     
                    <td  width="60" align="center" > 1 </td>
                    <td  width="80" align="center" >24/05/2019</td>
                    <td  width="80" align="center" >31/10/2019</td>
               
                    <td    align="right"  width="80">0.0</td>
                    <td    align="right" width="80" >0.0</td>
                    <td    align="right" width="120" >0.0</td>
                    <td    align="right" width="120" >0.0</td>
                   

                    
            
                </tr>
            </table>

        </td>
    
    </tr>


    <tr>
    <td colspan="5">
        &nbsp;

    </td>

</tr>

 
    
    
 




    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr class="border_top_light">
                     
                    <td  width="60" align="center" > </td>
                    <td  width="80" align="center" > </td>
                    <td  width="80" align="center" ><b>TOTAL</b></td>
               
                    <td    align="right"  width="80"><b>0.0</b></td>
                    <td    align="right" width="80" ><b>0.0</b></td>
                    <td    align="right" width="120"><b>0.0</b></td>
                    <td    align="right" width="120" ><b>0.0</b></td>
                   

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
    <td></td>
    </tr>

    <tr>
    <td></td>
    </tr>
 
 

 
</table>';









$t_datosDetalleMovimientosPeriodo = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DETALLE DE MOVIMIENTOS EN EL PERÍODO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                     
                    <td   class="border_light_td negrita" width="80" > Fecha  </td>
                    <td align="center"    class="border_light_td negrita" width="130" > Alternativa de Rendimiento </td>
                    <td    class="border_light_td negrita" width="180" > Descripción del Movimiento </td>
                    <td   class="border_light_td negrita" width="80"> Monto </td>
                    <td   class="border_light_td negrita" width="80"> Titulos </td>
                    <td align="center" class="border_light_td negrita" width="80"> Precio de titulo </td> 

                    
            
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    
                    <td     width="80" > 25/10/2019  </td>
                    <td   width="130" >  </td>
                    <td    width="180" > Costo cobertura(-)</td>
                    <td   align="right"  width="80"> 12.69 </td>
                    <td   align="right"  width="80">  </td>
                    <td   align="right" width="80">   </td> 

                    
            
                </tr>
            </table>

        </td>

    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    
                    <td     width="80" > 25/10/2019  </td>
                    <td   width="130" > SEG-SZ-STERGOB </td>
                    <td    width="180" > Venta de unidades(-)</td>
                    <td   align="right"  width="80"> 12.69 </td>
                    <td   align="right"  width="80"> 5.41 </td>
                    <td   align="right" width="80"> 2.344949  </td> 

                    
            
                </tr>
            </table>

        </td>

    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    
                    <td     width="80" > 25/10/2019  </td>
                    <td   width="130" >  </td>
                    <td    width="180" > Gastos de gestion(-)</td>
                    <td   align="right"  width="80"> 8,366.45 </td>
                    <td   align="right"  width="80">  </td>
                    <td   align="right" width="80">  </td> 

                    
            
                </tr>
            </table>

        </td>

    </tr>

    <tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr>
                
                <td     width="80" > 25/10/2019  </td>
                <td   width="130" >  SEG-SZ-STERGOB  </td>
                <td    width="180" > Venta de unidades(-) </td>
                <td   align="right"  width="80"> 8,366.45 </td>
                <td   align="right"  width="80"> 3,567.86  </td>
                <td   align="right" width="80"> 2.344949 </td> 

                
        
            </tr>
        </table>

    </td>

</tr>
    

    <tr>
    <td colspan="5">
        &nbsp;

    </td>

</tr>

<tr>
<td colspan="5">
    &nbsp;

</td>

</tr>


<tr>
<td colspan="5">
    &nbsp;

</td>

</tr>


 
    
    
 

 
 
</table>';






$t_datosDescripcionConceptos = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DESCRIPCION DE CONCEPTOS</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0"  >
                <tr>
                     
                    <td width="30%">Compra de títulos:</td><td width="70%">Indica el número de títulos que se adquirieron derivado del ingreso a la póliza.</td>
 
                </tr>

                <tr>
                        
                    <td width="30%">Costo de Cobertura:</td><td width="70%">Muestra el costo aplicado por concepto de mortalidad y gastos administrativos.</td>

                    
            
                </tr>
                <tr>
                        
                <td width="30%">Costo por administración:</td><td width="70%">Refiere al cargo se aplica a la póliza por la administración de alternativas de Rendimiento.</td>

                
        
            </tr>
                <tr>
                        
                    <td width="30%">Gasto de gestión:</td><td width="70%">Indica los movimientos de cargo al fondo, tales como: derecho de póliza, administración de alternatvias de rendimiento, entre otros.</td>

                    
            
                </tr>

                <tr>
                        
                    <td width="30%">Ingreso por Ajuste a la Reserva:</td><td width="70%">Se realiza un ajuste a la reserva en favor del cliente, equivalente a las retenciones de impuestos a los que está sujeta la misma. <b>IMPORTANTE: Este ajuste no produce ningún efecto fiscal al cliente y se realiza a la reserva de la aseguradora.</b></td>

                    
            
                </tr>
                <tr>
                        
                    <td colspan="2">&nbsp;</td>
            
                </tr>

                <tr>
                        
                <td colspan="2" class="negrita">NOTA IMPORTANTE: La información contenida en el presente documento, es únicamente de carácter informativo.
                
                </td>
        
            </tr>
            </table>

        </td>
    
    </tr>
   
<tr>
<td colspan="5">
    &nbsp;

</td>

</tr>


 
    
    
 

 
 
</table>';















$htmlCuerpo4 =  $estilo .   $t_datosAportacionesyRetirosHistorico . $t_datosDetalleMovimientosPeriodo . $t_datosDescripcionConceptos;

$pdf->writeHTML($htmlCuerpo4, true, false, true, false, '');





// add a page
















 


// -------------------------------------------------------------------

//Close and output PDF document
ob_clean();
 $pdf->Output('example_009.pdf', 'I');
 end_ob_clean();
//echo site_url();

//============================================================+
// END OF FILE
//============================================================+