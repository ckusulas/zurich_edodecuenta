<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<h1>Solicitud de Póliza</h1>

<!--Creamos un formulario que nos lleve al controlador formulario_controller/recibirFormulario-->
 
<br><br> 

    <table class="table" style="text-align:center;">
        <tr>
            <td>
            <form role="form" method="post" action="<?php echo site_url("/entrada/recibirFormulario");?>" >
            <div class="form-group">
                <label for="codigoProducto">Código del Producto</label>
                <input type="codigoProducto" name="codigoProducto" maxlength="5">
                <span class="help-block" style="color:red"><?php echo form_error('codigoProducto');?></span>

            </div>
            <div class="form-group">
                <label for="numeroPoliza">Numero de Póliza</label>
                <input type="numeroPoliza" name="numeroPoliza" maxlength="10">
                <span class="help-block" style="color:red"><?php echo form_error('numeroPoliza');?></span>

            </div>
            <div class="form-group">
                <label for="Periodo">Periodo</label>
                <select>
                        <option>Mensual</option>
                        <option>Trimestral</option>
                        <option>Anual</option>
                </select>
            </div>
            <input type="submit" value="Procesar Solicitud" class="btn btn-sucess" >
</form>
            </td>


        </tr>




    </table>






    </body>
</html>