<?php


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
 

    // Page footer
    public function Footer() {

        $this->SetY(-25);
        // Set font
        $this->SetFont('helvetica', ' ', 8);
        // Page number
        $this->Cell(0, 10, 'Av. Juan Salvador Agraz #73, pisos 3 y 4 Col. Santa Fe Cuajimalpa, Del.Cuajimalpa de Morelos, CP. 05348, CDMX, Mexico, Tel 51', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        $this->SetY(-22);
        // Set font
        $this->SetFont('helvetica', ' ', 8);
        // Page number
        $this->Cell(0, 10, '69 43 00 en la Cd. de Mexico y area meropolitana o lada sin costo 01 800 501 0000 del interior de la Republica.', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('UNIT LINKED SANTANDER');
$pdf->SetSubject('ESTADO DE CUENTA');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'UNIT LINKED SANTANDER', 'ESTADO DE CUENTA');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// -------------------------------------------------------------------

// add a page
$pdf->AddPage();
// create some HTML content
$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';
 $pdf->SetFont('helvetica', ' ', 10);

$estilo = '
    <style>
        tr.border_bottom td {
            border-bottom:1pt solid black;
        }

        tr.border_Subbottom td {
            border-bottom:0.5pt solid black;
          }


          span.smalltext {
            font-size: .8em; /* .8em x 10px = 8px */
          }
    </style> ';

$encabezado = '

    <br><h4>Zurich Santander Seguros México S.A.</h4>';


$tabla1 = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="2"><b>DATOS DEL CONTRATANTE</b></td>
        <td colspan="2" align="right">PERIODO DEL: <b>01/10/2019</b> AL <b>31/10/2019</b></td>
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>CONTRATANTE:</b></td>
                    <td width="220">ANTONIO</td>
                    <td width="150"><b>NUMERO DE PÓLIZA:</b></td>
                    <td>1 102 00000012020</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>ASEGURADO:</b></td>
                    <td width="220">ANTONIO</td>
                    <td width="150"><b>PRODUCTO:</b></td>
                    <td>UNIT LINKED SATANDER</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>DOMICILIO:</b></td>
                    <td width="220">SONORA</td>
                    <td width="150"><b>MONEDA:</b></td>
                    <td>PESOS</td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE EMISIÓN:</b></td>
                    <td>24/05/2019</td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE VENCIMIENTO:</b></td>
                    <td>19/05/2019</td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE CORTE:</b></td>
                    <td>31/10/2019</td>
                </tr>
            </table>

        </td>
    </tr>
     
     
   
</table>';


$tabla2 = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DATOS DEL BANQUERO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>EJECUTIVO:</b></td>
                    <td width="150">JOSE DANIEL</td>
                    <td width="160"><b>CORREO ELECTRÓNICO:</b></td>
                    <td width="180">constantinokv@santander.com</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>CÓDIGO:</b></td>
                    <td width="150"> </td>
                    <td width="160"><b>TELÉFONO:</b></td>
                    <td width="180"></td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>SUCURSAL:</b></td>
                    <td width="150">CONS. AREA DE NEGOCIO</td>
                    <td width="150"> </td>
                    <td> </td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
     
     
   
</table>';

$tabla4 = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>RESUMEN DE MOVIMIENTOS DEL PERÍODO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    
    <tr>
        <td width="80"></td>
        <td colspan="4">
            <table border="0" width="500">

                <tr class="border_Subbottom">
                        
                    <td width="250">Saldo al Inicio</td>
                    <td width="150" align="right">13,217,783.89</td>
            
                </tr>
                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(+) Ingreso </td>
                    <td width="150" align="right">0.00</td>
                
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Aportaciones </td>
                    <td width="150" align="right">0.00</td>
            
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Ingreso por Ajuste a la Reserva </td>
                    <td width="150" align="right">0.00</td>
        
                </tr>


                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(-) Egreso </td>
                    <td width="150" align="right">8,366.45</td>
                
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Costo del Seguro y Derecho de Póliza </td>
                    <td width="130" align="right">12.69</td>
        
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Cargos a la Póliza</td>
                    <td width="130" align="right">8,366.45</td>
    
                 </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Retiros</td>
                    <td width="130" align="right">0.00</td>
 
                </tr>


                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Impuesto SObre la Renta</td>
                    <td width="130" align="right">0.00</td>
 
                </tr>
                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(+) Rendimientos </td>
                    <td width="150" align="right">84,092.68</td>
                
                </tr>


                <tr class="border_Subbottom">
                            
                    <td width="250">Saldo al Cierre**</td>
                    <td width="150" align="right">13,393,497.43</td>
        
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                

                
            </table>

        </td>

    </tr>



    <tr>
   
    <td colspan="4">
        <table border="0" width="600">
          <tr>
                <td>
                        <span class="smalltext">
                        ** La Aseguradora con base en el articulo 93, Fraccion XXI de LISR pagara el rendimiento sin retencion de impuesto, siempre y 
                        cuando el asegurado cumpla con los siguiente:  </span>  
                        <span class="smalltext"> <br> 
                            a) A la fecha de rescate tenga 60 anos de edad o mas.
                            <br>
                            b) Que hayan transcurrido al menos 5 anos desde la fecha de contratacion del seguro y el momento en que se pague el rescate.
                            <br>
                            c)Que el Asegurado sea el Contratante de la Poliza.
                        </span>

                        <br>

                        <span class="smalltext">             <br>
                        La Aseguradora emitira la constancia de retencion de impuestos por el 20% sobre interes real cuando el Asegurado rescata su poliza, y no cumpla con ,os requisitos anteriormente citados.
                        </span>


                        <br>

                        <span class="smalltext">             <br>
                       Sujeto a lo previsto en la normatividad vigente al momento del pago del rendimiento.
                        </span>


                      
                </td>
            </tr>
        </table>

    </td>

</tr>


    

   

 
     
     
   
</table>';


$htmlCuerpo1 = $estilo . $encabezado . $tabla1 . $tabla2 . $tabla4;

// output the HTML content
$pdf->writeHTML($htmlCuerpo1, true, false, true, false, '');

 


// -------------------------------------------------------------------

//Close and output PDF document
ob_clean();
 $pdf->Output('example_009.pdf', 'I');
 end_ob_clean();
//echo site_url();

//============================================================+
// END OF FILE
//============================================================+