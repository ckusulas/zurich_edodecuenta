<?php







// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
        //Page header
        public function Header() {
            // Logo
            $image_file = K_PATH_IMAGES.'zurich-logo.jpg';
            $this->Image($image_file, 15, 3, 80, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            // Set font
    
            $this->SetY(13);
            $this->SetFont('helvetica', 'B', 10);
           
            // Producto 
            $this->Cell(-25, 25, 'OBJETIVO PROTEGIDO SANTANDER', 0, false, 'R', 0, '', 0, false, 'M', 'M');
            $this->SetY(20);
            $this->SetFont('helvetica', 'B', 10);
            // Subtitle
             $this->Cell(0, 0, 'ESTADO DE CUENTA', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        }
 

    // Page footer
    public function Footer() {

        $this->SetY(-25);
        // Set font
        $this->SetFont('helvetica', ' ', 8);
        // Page number
        $this->Cell(0, 10, 'Av. Juan Salvador Agraz #73, pisos 3 y 4 Col. Santa Fe Cuajimalpa, Del.Cuajimalpa de Morelos, CP. 05348, CDMX, Mexico, Tel 51', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        $this->SetY(-22);
        // Set font
        $this->SetFont('helvetica', ' ', 8);
        // Page number
        $this->Cell(0, 10, '69 43 00 en la Cd. de Mexico y area meropolitana o lada sin costo 01 800 501 0000 del interior de la Republica.', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('OBJETIVO PROTEGIDO SANTANDER');
$pdf->SetSubject('ESTADO DE CUENTA');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData('', 120, 'UNIT LINKED SANTANDER', 'ESTADO DE CUENTA');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// -------------------------------------------------------------------

// add a page
$pdf->AddPage();
// create some HTML content
$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';
 $pdf->SetFont('Helvetica', ' ', 10);

$estilo = '
    <style>
        tr.border_bottom td {
            border-bottom:1pt solid red;
        }


        tr.border_top td {
            border-top:1pt solid red;
          }

          tr.border_top_light td {
            border-top:0.5pt solid black;
          }

          td.border_top_light_td {
            border-top:0.5pt solid black;
          }
          td.border_light_td {
            border:0.5pt solid black;
          }

          td.border_top_light {
            border:0.5pt solid black;
           
          
          }

     


        tr.border_Subbottom td {
            border-bottom:0.5pt solid black;
          }


          span.smalltext {
            font-size: .8em; /* .8em x 10px = 8px */
          }

          td.smalltext {
            font-size: .8em; /* .8em x 10px = 8px */
          }

          
          span.normaltext {
            font-size: .12em; /* .8em x 10px = 8px */
          }

          td.normaltext {
            font-size: .12em; /* .8em x 10px = 8px */
          }

          td.negrita {
            font-weight: bold;
          }

 
    </style> ';

$titulo = '

    <table>
        <tr class="border_top"><td></td></tr>
  
        <tr>
        <td><h4>Zurich Santander Seguros México S.A.</h4></td></tr>
        <tr><td></td></tr>
    </table>'; 

 

$t_datosContratante = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="2"><b>DATOS DEL CONTRATANTE</b></td>
        <td colspan="2" align="right">PERIODO DEL: <b>'.$FechaInicial.'</b> AL <b>'.$FechaFinal.'</b></td>
        
    </tr>
    <tr >
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>CONTRATANTE:</b></td>
                    <td width="220">' . $ContratanteNombre . '</td>
                    <td width="150"><b>NÚMERO DE PÓLIZA:</b></td>
                    <td> 1 ' . $Ramo . ' ' . str_pad($noPoliza, 10, "0",STR_PAD_LEFT).'</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>ASEGURADO:</b></td>
                    <td width="220">' . $Asegurado . '</td>
                    <td width="150"><b>PRODUCTO:</b></td>
                    <td>' . $Producto . '</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>DOMICILIO:</b></td>
                    <td width="220">'.$DireccionContratante.'</td>
                    <td width="150"><b>MONEDA:</b></td>
                    <td>'.$Moneda.'</td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE EMISIÓN:</b></td>
                    <td>'.$FechaEmision.'</td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE VENCIMIENTO:</b></td>
                    <td>'.$FechaVencimiento.'</td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE CORTE:</b></td>
                    <td>'.$FechaFinal.'</td>
                </tr>
            </table>

        </td>
    </tr>
     
     
   
</table>';

$wd_ejecutivo_label = 120;
$wd_ejecutivo_data = 230;
$wd_correo_label = 160;
$wd_correo_data = 160;



//$EjecutivoNombre

$t_datosBanquero = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DATOS DEL BANQUERO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="10"> </td>
                    <td width="120"><b>EJECUTIVO:</b></td>
                    <td width="230">'.$EjecutivoNombre.'</td>
                    <td width="160"><b>TELÉFONO:</b></td>
                    <td width="100">'.$EjecutivoTelefono.'</td>
                 
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="10"> </td>
                    <td width="'.$wd_ejecutivo_label.'"><b>CÓDIGO:</b></td>
                    <td width="'.$wd_ejecutivo_data.'">'.$EjecutivoCodigo.'</td>
                    <td width="'.$wd_correo_label.'"><b>CORREO ELECTRÓNICO:</b></td>
                    <td width="'.$wd_correo_data.'">'.$EjecutivoCorreo.'</td>
                   
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="10"> </td>
                    <td width="'.$wd_ejecutivo_label.'"><b>SUCURSAL:</b></td>
                    <td width="'.$wd_ejecutivo_data.'">'.$EjecutivoSucursal.'</td>
                    <td width="'.$wd_correo_data.'"> </td>
                    <td> </td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
     
     
   
</table>';

$t_coberturasAmparadas = '
<table border="0" width="770" bgcolor="">
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>COBERTURAS AMPARADAS</b></td>
        
        
    </tr>
    <tr>
       <td colspan="5" >
          <table border="0" width="100%">
                <tr>
                    <td> </td>
                    <td> </td>
                    <td align="center"><b>Suma Asegurada</b></td>
                </tr>
            </table>
        </td>
    </tr>
 

    <tr>
    <td colspan="5" >
        <table  width="740" border="0" >
                <tr>
                    <td width="3"></td>
                    <td></td>
                    <td width="305"> </td>
                    <td width="100" align="right" class="border_top_light_td"><b>Alcanzada</b></td>
                    <td width="100" align="right" class="border_top_light_td" ><b>Contratada</b></td>
                </tr>
            </table>
    </td>
</tr>

    <tr>
        <td colspan="5" >
            <table    width="740" border="0" >
                    <tr class="border_top_light"> 
                        <td width="3"></td>
                        <td><b>Cobertura Básica:</b></td>
                        <td width="305">'.$CoberturaBasicaTxt.'</td>
                        <td width="100" align="right">'. number_format($CoberturaBasicaAlcanzada,2).'</td>
                        <td width="100" align="right">'. number_format($CoberturaBasicaContratada,2).'</td>
                    </tr>
                </table>
        </td>
    </tr>
    <tr>
    <td colspan="5" >
        <table    width="740" border="0" >
                <tr class=""> 
                    <td width="3"></td>
                    <td><b>Cobertura Adicionales:</b></td>
                    <td width="305">'.$CoberturaAdicionalTxt.'</td>
                    <td width="100" align="right">'.number_format($CoberturaAdicionalAlcanzada,2).'</td>
                    <td width="100" align="right">'.number_format($CoberturaAdicionalContratada,2).'</td>
                </tr>
            </table>
    </td>
</tr>



    
    <tr>
     <td colspan="5"></td>
    </tr>
        
 

 
     
     
   
</table>';

$t_resumenPeriodo = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>RESUMEN DE MOVIMIENTOS DEL PERÍODO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    
    <tr>
        <td width="80"></td>
        <td colspan="4">
            <table border="0" width="500">

                <tr class="border_Subbottom">
                        
                    <td width="250">Saldo al Inicio</td>
                    <td width="150" align="right">'.number_format($MP_SaldoInicio,2).'</td>
            
                </tr>
                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(+) Ingreso </td>
                    <td width="150" align="right"> '.number_format($MP_Ingreso,2).'</td>
                
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Aportaciones </td>
                    <td width="150" align="right">'.number_format($MP_Aportaciones,2).'</td>
            
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Ingreso por Ajuste a la Reserva </td>
                    <td width="150" align="right">'.number_format($MP_IngresoAjusteReserva,2).'</td>
        
                </tr>


                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(-) Egreso </td> 
                    <td width="150" align="right">'.number_format($MP_Egreso,2).'</td>
                
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Costo del Seguro y Derecho de Póliza </td>
                    <td width="130" align="right">'.number_format($MP_CostoSeguroDerechoPoliza,2).'</td>
        
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Cargos a la Póliza</td>
                    <td width="130" align="right">'.number_format($MP_CargosPoliza,2).'</td>
    
                 </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Retiros</td>
                    <td width="130" align="right"> '.number_format($MP_Retiros,2).'</td>
 
                </tr>


                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Impuesto Sobre la Renta</td>
                    <td width="130" align="right"> '.number_format($MP_ISR,2).'</td>
 
                </tr>
                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(+) Rendimientos </td>
                    <td width="150" align="right"> '.number_format($MP_Rendimientos,2).'</td>
                
                </tr>


                <tr class="border_Subbottom">
                            
                    <td width="250">Saldo al Cierre**</td>
                    <td width="150" align="right"> '.number_format($MP_SaldoCierre,2).'</td>
        
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                

                
            </table>

        </td>

    </tr>



    <tr>
   
    <td colspan="4">
        <table border="0" width="600">
          <tr>
                <td class="smalltext" >
                        ** La Aseguradora con base en el Artículo 93, Fracción XXI de LISR pagara el rendimiento sin retención de impuesto, siempre y   </td>
            </tr>
            <tr>

            <td class="smalltext" >
                        
                        
                        
                        cuando el asegurado cumpla con los siguiente: 
                        </td> 
            </tr>
            <tr>
                <td class="smalltext">
                            a) A la fecha de rescate tenga 60 años de edad o más.
                            <br>
                            b) Que hayan transcurrido al menos 5 años desde la fecha de contratación del seguro y el momento en que se pague el rescate.
                            <br>
                            c) Que el Asegurado sea el Contratante de la Póliza.
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            
            <tr>
                <td class="smalltext">
                        La Aseguradora emitirá la constancia de retención de impuestos por el 20% sobre interés real cuando el Asegurado rescate su póliza, 
                </td>
                
                </tr>
            <tr>
                <td class="smalltext">
                 y no cumpla con los requisitos anteriormente citados.
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>



            <tr>
                <td class="smalltext">
                       Sujeto a lo previsto en la normatividad vigente al momento del pago del rendimiento.
                </td>
            </tr>
        </table>

    </td>

</tr>
<tr>
<td>&nbsp;</td>
</tr>


    

   

 
     
     
   
</table>';


$htmlCuerpo1 = $estilo . $titulo . $t_datosContratante . $t_datosBanquero . $t_coberturasAmparadas;

// output the HTML content
$pdf->writeHTML($htmlCuerpo1, true, false, true, false, '');




$ejecutivo_label = 130;
$ejecutivo_data = 150;
$correo_label = 160;
$correo_data = 180;

 


/////////////////////////////////////////////////////////////////////////////////////////////////  PAGE 2




$t_datosResumenRendimientosAlternativa = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>RESUMEN DE RENDIMIENTOS</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    <td width="32"  > </td>
                    <td align="center" class="border_top_light" ><b>Alternativa de Rendimiento </b> </td>
                    <td align="center" class="border_top_light"><b>Distribucion de Portafolio</b></td>
                    <td class="border_top_light" >
                        <table   cellspacing=0 style="width: 100%; padding:0px; margin: 0px;">
                       
                            <tr>
                                <td colspan="2"   align="center"  ><b>Precio Titulo</b></td>
                            </tr>
                            <tr>
                                <td align="center"  ><b>Inicio</b></td><td align="center" ><b>Fin</b></td>
                            </tr>
                        </table>
                    </td>

                    <td align="center" width="190" class="border_top_light">
                        <b>Tasa de Rendimiento </b> <sup>(2)</sup>
                    </td>
            
                </tr>
            </table>

        </td>
    
    </tr>

    
   
<tr>
 
 
<td colspan="5">


    <table border="0" width="740">
        <tr>
            <td width="32"> </td>
            <td align="center" >'.$RRA_AlternativaRendimiento1.'</td>
            <td align="center">'.$RRA_DistribucionPortafolio1.'</td>
            <td>
                <table>
                
                    <tr>
                        <td align="center">'.$RRA_PrecioTitulo_Inicio1.'</td><td align="center">'.$RRA_PrecioTitulo_Fin1.'</td>
                    </tr>
                </table>
            </td>

            <td align="center">
               '.$RRA_TasaRendimiento1.'% <sup>(a)</sup>
                            </td>
    
        </tr>
 
    </table>

</td> 

</tr>
<tr>
<td colspan="5"> </td>
       
</tr>

<tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr>
                
                <td>
                    <table border="0">
                        <tr>
                            <td  class="smalltext" width="20">   
                                (1) 
                            </td>
                            <td class="smalltext" width="650" >
                                Porcentaje de distribucion de Saldo a la fecha de corte, corrrepsondiente a la proprocion de cada una de la alternativas de rendimiento.
                            
                            </td>


                        </tr>

                        <tr>
                            <td  class="smalltext" width="20">   
                                (2)  
                            </td>
                            <td class="smalltext"  width="600">
                                (a) Tasa efectiva anual en el periodo para Alternativas de renta fija. (b) Rendimiento directo para alternativas en renta variable.
                            
                            </td>

                        </tr>
                
                
                
                                
                    </table>
                
                </td>
        
            </tr>
        </table>

    </td>

</tr>

<tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
</table>';



$total_TAR_Inicio = $TAR_Inicio1 + $TAR_Inicio2;
$total_TAR_ingresos = $TAR_Ingresos1 + $TAR_Ingresos2;
$total_TAR_costoSeguro = $TAR_CostosSeguro1 + $TAR_CostosSeguro2;
$total_TAR_otrosEgresos = $TAR_OtrosEgresos1 + $TAR_OtrosEgresos2;
$total_TAR_TitulosCierre = $TAR_TitulosCierre1 + $TAR_TitulosCierre2;


$t_datosAlternativasRendimiento = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>TITULOS DE ALTERNATIVAS DE RENDIMIENTO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                     
                    <td align="center"  width="120"  class="border_top_light" ><b>Alternativa de Rendimiento </b> </td>
                    <td align="center"   width="120"  class="border_top_light"><b>% Distribucion de Ingresos</b></td>
                    <td align="center" class="border_top_light"><b>Inicio</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(+) Ingresos</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Costos del Seguro</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Otros Egresos</b></td>
                    <td align="center" class="border_top_light" width="100"><b>Titulos al Cierre</b></td>

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">

       
       
            <table border="0" width="740">
                <tr class="">
                     
                    <td  width="120"  >'.$TAR_AlternativaRendimiento1.'</td>
                    <td  align="right" >'.$TAR_DistribucionIngresos1 .'%</td>
                    <td align="right"  >'.number_format($TAR_Inicio1,2).'</td>
                    <td    width="70" align="right">'.number_format($TAR_Ingresos1,2).'</td>
                    <td    width="70" align="right">'.number_format($TAR_CostosSeguro1,2).'</td>
                    <td    width="70" align="right">'.number_format($TAR_OtrosEgresos1,2).'</td>
                    <td    width="100" align="right">'.number_format($TAR_TitulosCierre1,2).'</td>

                    
            
                </tr>
                
            </table>
      

        </td>
    
    </tr>

    <tr>
    <td colspan="5">
        <table border="0" width="740"  >
            <tr  class="border_top_light">
                 
                   
            <td  width="120"  >  </td>
            <td  align="right"  ><b>TOTAL TITULOS</b></td>
                
                <td align="right" class="negrita"  > '.number_format($total_TAR_Inicio,2).'</td>
                <td    width="70" class="negrita"  align="right">'.number_format($total_TAR_ingresos,2).'</td>
                <td    width="70" align="right" class="negrita" >'.number_format($total_TAR_costoSeguro,2).'</td>
                <td    width="70" align="right" class="negrita" >'.$total_TAR_otrosEgresos.'</td>
                <td    width="100" align="right" class="negrita" > '.$total_TAR_TitulosCierre.' </td>

                
        
            </tr>
        </table>

    </td>

</tr>

    
    

<tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
</table>';



$total_SAR_SaldoInicio = $SAR_SaldoInicio1 + $SAR_SaldoInicio2 + $SAR_SaldoInicio3 + $SAR_SaldoInicio4 + $SAR_SaldoInicio5 +  $SAR_SaldoInicio6 + $SAR_SaldoInicio7;
$total_SAR_Ingresos = $SAR_Ingresos1 + $SAR_Ingresos2 + $SAR_Ingresos3 + $SAR_Ingresos4 + $SAR_Ingresos5 + $SAR_Ingresos6 + $SAR_Ingresos7;
$total_SAR_CostoSeguro = $SAR_CostoSeguro1 + $SAR_CostoSeguro2 + $SAR_CostoSeguro3 + $SAR_CostoSeguro4 + $SAR_CostoSeguro5 + $SAR_CostoSeguro6 + $SAR_CostoSeguro7;
$total_SAR_OtrosEgresos = $SAR_OtrosEgresos1 + $SAR_OtrosEgresos2 + $SAR_OtrosEgresos3 + $SAR_OtrosEgresos4 + $SAR_OtrosEgresos5 + $SAR_OtrosEgresos6 + $SAR_OtrosEgresos7;
$total_SAR_Rendimientos = $SAR_Rendimientos1 + $SAR_Rendimientos2 + $SAR_Rendimientos3 + $SAR_Rendimientos4 + $SAR_Rendimientos5 + $SAR_Rendimientos6 + $SAR_Rendimientos7;
$total_SAR_SaldosCierre = $SAR_SaldosCierre1 + $SAR_SaldosCierre2 + $SAR_SaldosCierre3 + $SAR_SaldosCierre4 + $SAR_SaldosCierre5 + $SAR_SaldosCierre6 + $SAR_SaldosCierre7;



$t_datosAlternativasRendimientoMXN = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>SALDO DE ALTERNATIVAS DE RENDIMIENTO(MXN)</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                     
                    <td align="center"  width="120"  class="border_top_light" ><b>Alternativa de Rendimiento </b> </td>
                    <td align="center"   width="120"  class="border_top_light"><b>Saldo al Inicio</b></td>
                    <td align="center" class="border_top_light"><b>(+) Ingresos</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Costos del Seguro</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Otros Egresos</b></td>
                    <td align="center" class="border_top_light" width="100"><b>(+) Rendimientos</b></td>
                    <td align="center" class="border_top_light" width="100"><b>Saldos al Cierre</b></td>

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
         
 
        <td colspan="5">
            <table border="0" width="740">
                <tr class="">
                     
                    <td  width="120"  >'.$SAR_AlternativaRendimiento1.'</td>
                    <td  align="right" >'.number_format($SAR_SaldoInicio1,2).'</td>
                    <td align="right"  >'.number_format($SAR_Ingresos1,2).'</td>
                    <td    width="70" align="right">'.number_format($SAR_CostoSeguro1,2).'</td>
                    <td    width="70" align="right">'.number_format($SAR_OtrosEgresos1,2).'</td>
                    <td    width="100" align="right">'.number_format($SAR_Rendimientos1,2).'</td>
                    <td    width="100" align="right">'.number_format($SAR_SaldosCierre1,2).'</td>

                    
            
                </tr>
               
   
            </table>


        </td>

       
    
    </tr>

    <tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr class="border_top_light" >
                 
                <td  width="120"  ><b>TOTAL SALDO</b></td>
                <td  align="right" class="negrita" >'.number_format($total_SAR_SaldoInicio,2).'</td>
                <td align="right" class="negrita" >'.number_format($total_SAR_Ingresos,2).'</td>
                <td    width="70" align="right" class="negrita">'.number_format($total_SAR_CostoSeguro,2).'</td>
                <td    width="70" align="right" class="negrita">'.number_format($total_SAR_OtrosEgresos,2).'</td>
                <td    width="100" align="right" class="negrita">'.number_format($total_SAR_Rendimientos,2).'</td>
                <td    width="100" align="right" class="negrita">'.number_format($total_SAR_SaldosCierre,2).'</td>

                
        
            </tr>
        </table>

    </td>

</tr>
    
    

<tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>

    <tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr>
                
                <td>
                    <table border="0">
                        <tr>
                            <td  class="smalltext" width="20">   
                                (3) 
                            </td>
                            <td class="smalltext" width="650" >
                               Porcentaje actual de distribución de ingresos, el cual es utilizado para distribuir las aportaciones de prima recibidos en la póliza.
                            </td>


                        </tr>

                        <tr>
                            <td  class="smalltext" width="20">   
                                (4)  
                            </td>
                            <td class="smalltext"  width="600">
                               Cargos aplicados por concepto mortalidad, costo de administración y derecho de póliza, tal como se específica en las condiciones generales del producto.
                            </td>

                        </tr>
                

                        <tr>
                        <td  class="smalltext" width="20">   
                            (5)  
                        </td>
                        <td class="smalltext"  width="600">
                         Retiros efectuados a petición del Contratante, impuestos, costo de administración de cada alternativa de rendimiento y cargos generados por movimientos realizados por el cliente.                    </td>

                    </tr>
            
                
                
                                
                    </table>
                
                </td>
        
            </tr>
        </table>

    </td>

</tr>
 <tr>
 <td>&nbsp;</td>
 </tr>

 
</table>';



// add a page
$pdf->AddPage();









$htmlCuerpo2 = $estilo .  $t_resumenPeriodo. $t_datosResumenRendimientosAlternativa . $t_datosAlternativasRendimiento;

$pdf->writeHTML($htmlCuerpo2, true, false, true, false, '');





// add a page
$pdf->AddPage();





$t_datosPresentarSolicitudDeAclaracionoServicio = '
<table border="0" width="770"  >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>PARA PRESENTAR UNA SOLICITUD DE ACLARACIÓN O SERVICIO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
    <td colspan="5">
            <table border="0" width="100%">
                <tr>
                    
                    <td>
                        <table border="0"   >
                            <tr>
                               
                                <td class="smallnormaltexttext" width="*" >
                                <b>UNIDAD ESPECIALIZADA PARA LA ATENCION DE USUARIOS (UEA)</b>
                                <br>
                                <br>
                                Av. Juan Salvador Agraz #73, piso 3, Col. Santa Fe Cuajimalpa, Del. Cuajimalpa de Morelos, C.P. 05348, Ciudad
                                <br> 
                                de México Correo Electrónico: ueaseguros@santander.com.mx
                                <br>
                                <br>
                                Teléfonos: 55 1037-3500 Ext. 13566, 13571 y 13515, con un horario de atención de lunes a jueves de 7:30 a 
                                <br>
                                17:00 horas y viernes de 7:30 a 14:00 horas.
                                <br>
                                <br>
                                <b>CONDUSEF</b>
                                <br>
                                <br>
                                Av. Insurgentes Sur #762, planta baja, Col. Del Valle, Del. Benito Juarez, C.P.03100, Ciudad de México.
                                <br>
                                Correo Electrónico: asesoria@condusef.gob.mx, Pagina Web: http://www.cundusef.gob.mx, Teléfonos: 01 800 
                                <br>
                                999 8080 o 5340 0999 

                                </td>


                            </tr>
 
                
                    
                    
                                    
                        </table>
                    
                    </td>
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5"> </td>
       
    </tr>



</table>';



    $t_datosComisionesAplicadasPromedio = '

    <table border="0" width="770" >
        
        <tr class="border_bottom">
            <td width="40"> </td>
            <td colspan="4"><b>COMISIONES PROMEDIO APLICADAS DE SIEFORES Y FONDOS DE INVERSIÓN</b></td>
        </tr>


        <tr>
        <td colspan="5"> </td>
        </tr>
        <tr>
        <td colspan="5"> </td>
        </tr>
        
        
        <tr>

        
        <td  width="750" colspan=5 >



<table width="100%" border="0">

<tr>

  <td width="200"><table width="100%" border="0">

  <tr>
  <td colspan="2" class="smalltext" align="center"> Comisiones de las Siefores Básicas (Al cierre de Diciembre de 2017) <br>
    </td>
  </tr>

    <tr bgcolor="#333333">

      <td width="56%" class="smalltext"><div align="center" ><br><br><br><font color="White"><b> Afore</b></font></div></td>

      <td width="44%"><table width="100%" border="1">

        <tr>

          <td class="smalltext" height="60"><div align="center">  <br><br><font color="white"><b>Porcentaje</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext"><div align="center"><font color="white"><b>Anual</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext"><div align="center"><font color="white"><b>Sobre Saldo</b></font></div></td>

        </tr>

      </table></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Azteca</b></td>

      <td class="smalltext border_light_td" ><div align="right">1.100</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Banamex</b></td>

      <td class="smalltext border_light_td"><div align="right">0.990</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Coppel</b></td>

      <td class="smalltext border_light_td"><div align="right">1.100</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Inbursa</b></td>

      <td class="smalltext border_light_td"><div align="right">0.980</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Invercap</b></td>

      <td class="smalltext border_light_td"><div align="right">1.100</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Metlife</b></td>

      <td class="smalltext border_light_td"><div align="right">1.100</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>PensionISSSTE</b></td>

      <td class="smalltext border_light_td"><div align="right">0.860</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Principal</b></td>

      <td class="smalltext border_light_td"><div align="right">1.090</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Profuturo GNP</b></td>

      <td class="smalltext border_light_td"><div align="right">1.030</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>SURA</b></td>

      <td class="smalltext border_light_td"><div align="right">1.030</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>XXI Banorte</b></td>

      <td class="smalltext border_light_td"><div align="right">1.000</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>Promedio</b></td>

      <td class="smalltext border_light_td"><div align="right">1.035</div></td>

    </tr>

  </table></td>

  <td width="33">&nbsp;</td>

  <td width="400"><table  width="100%" border="0">

    <tr>

      <td class="smalltext" align="center"> Comisiones de los Productos de Ahorro IV 5 <br>

        (Al Cierre de Diciembre de 2017)<br></td>

    </tr>

    <tr>

      <td class="smalltext"><table width="100%" border="1">

        <tr bgcolor="#333333">

          <td class="smalltext" width="60"><div align="center"><br><br><br><font color="white"><b>No.Fondo</b></font></div></td>

          <td class="smalltext" width="230"><div align="center"><br><br><br><br><font color="white"><b>NOM_FONDO</b></font></div></td>

          <td class="smalltext" width="50"><div align="center"><font color="white"><b>Porcentaje sobre el saldo antes de la renovación.</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext" align="center">1</td>

          <td class="smalltext">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext"  align="center">3</td>

          <td class="smalltext">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="right">1.100%</div></td>

        </tr>

        <tr>

          <td class="smalltext"  align="center">4</td>

          <td class="smalltext">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext"  align="center">6</td>

          <td class="smalltext">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="right">1.150%</div></td>

        </tr>

        <tr>

          <td class="smalltext" >&nbsp;</td>

          <td class="smalltext"  align="center"><b>Promedio</b></td>

          <td class="smalltext"><div align="right"></div></td>

        </tr>

        <tr>

          <td class="smalltext" align="center">2</td>

          <td class="smalltext">FONDO SANTANDER IV 2 - MIXTO</td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext" align="center">5</td>

          <td class="smalltext">FONDO SANTANDER IV 2 - MIXTO</td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext">&nbsp;</td>

          <td class="smalltext"  align="center"><b>Promedio</b></td>

          <td class="smalltext"><div align="right">1.000%</div></td>

        </tr>

      </table></td>

    </tr>

    <tr>

      <td class="smalltext" align="center"> <br><br>Comisiones de las Sociedades de Inversión<br>

        (Al cierre de Sepitembre de 2015)<br></td>

    </tr>

    <tr>

      <td class="smalltext"><table width="100%" border="0">

        <tr bgcolor="#333333">

          <td class="smalltext"  width="60"><div align="center"><font color="white"><br><b>S.INVERSION</b></font> </div></td>

          <td class="smalltext"  width="230"><div align="center"><font color="white"><br><b>EMISORA</b></font></div></td>

          <td class="smalltext"  width="60"><div align="center"  ><font color="white"><b>Porcentaje Anual Sobre Saldo</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">IXELQ BF2</td>

          <td class="smalltext border_light_td">FONDO DE BANORTEIXE (GUB/PRIVADOS)</td>

          <td class="smalltext border_light_td"><div align="right">1.500%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">BMER180 B2</td>

          <td class="smalltext border_light_td">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext border_light_td"><div align="right">1.939%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">BMER30 B2</td>

          <td class="smalltext border_light_td">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext border_light_td"><div align="right">1.939%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">BMER90 B2</td>

          <td class="smalltext border_light_td">FONDO SANTANDER IV 1 - RENTA FIJA</td>

          <td class="smalltext border_light_td"><div align="right">1.939%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">&nbsp;</td>

          <td class="smalltext border_light_td"><div align="center"><b>Promedio</b></div></td>

          <td class="smalltext border_light_td"><div align="right">1.829%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">ACTIVCO B-2</td>

          <td class="smalltext border_light_td">FONDO DE ACTINVER (RENTA FIJA/VARIABLE)</td>

          <td class="smalltext border_light_td"><div align="right">1.068%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">DIVER-CP</td>

          <td class="smalltext border_light_td">FONDO DE BBVA BANCOMER (RENTA FIJA/VARIABLE)</td>

          <td class="smalltext border_light_td"><div align="right">1,989%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">SURV10 BF2</td>

          <td class="smalltext border_light_td">FONDO DE SURA(RENTA FIJA/VARIABLE)</td>

          <td class="smalltext border_light_td"><div align="right">1.700</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">&nbsp;</td>

          <td class="smalltext border_light_td"><div align="center"><b>Promedio</b></div></td>

          <td class="smalltext border_light_td"><div align="right">1.586%</div></td>

        </tr>

      </table></td>

    </tr>

    <tr>

      <td class="smalltext">&nbsp; </td>

    </tr>

    <tr>

      <td class="smalltext">&nbsp;</td>

    </tr>

  </table></td>

</tr>

  </table></td>

</tr>

</table>


          
        
        </td>
        
        </tr>
    
    

    
    </table>';


$htmlCuerpo3 = $estilo . $t_datosAlternativasRendimientoMXN . $t_datosPresentarSolicitudDeAclaracionoServicio;

$pdf->writeHTML($htmlCuerpo3, true, false, true, false, '');




$pdf->AddPage();


//////////////////////////////////////////////////////////////////////////////////  Page 4



$total_ARH_PrimaPactada = $ARH_PrimaPactada1 + $ARH_PrimaPactada2 + $ARH_PrimaPactada3 + $ARH_PrimaPactada4 + $ARH_PrimaPactada5;
$total_ARH_PrimasPagadas = $ARH_PrimasPagadas1 + $ARH_PrimasPagadas2 + $ARH_PrimasPagadas3 + $ARH_PrimasPagadas4 + $ARH_PrimasPagadas5;
$total_ARH_PrimaAdicionalPagada = $ARH_PrimaAdicionalPagada1 + $ARH_PrimaAdicionalPagada2 + $ARH_PrimaAdicionalPagada3 + $ARH_PrimaAdicionalPagada4 + $ARH_PrimaAdicionalPagada5;
$total_ARH_RetirosEfectuados = $ARH_RetirosEfectuados1 + $ARH_RetirosEfectuados2 + $ARH_RetirosEfectuados3 + $ARH_RetirosEfectuados4 + $ARH_RetirosEfectuados5;



$t_datosAportacionesyRetirosHistorico = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>APORTACIONES Y RETIROS (HISTORICO)</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                     
                    <td align="center"  class="border_top_light" width="60" ><b>Año Póliza</b> </td>
                    <td align="center"  class="border_top_light" width="80" ><b>Fecha Desde</b></td>
                    <td align="center"  class="border_top_light" width="80" ><b>Fecha Hasta</b></td>
                    <td align="center" class="border_top_light" width="80"><b>Prima Pactada</b></td>
                    <td align="center" class="border_top_light" width="80"><b>Primas Pagadas</b></td>
                    <td align="center" class="border_top_light" width="120"><b>Prima Adicional Pagada</b></td>
                    <td align="center" class="border_top_light" width="120"><b>Retiros Efectuados</b></td>

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr class=" ">
                     
                    <td  width="60" align="center" >'.$ARH_AnioPoliza1.'</td>
                    <td  width="80" align="center" >'.$ARH_FDesde1.'</td>
                    <td  width="80" align="center" >'.$ARH_FHasta1.'</td>
               
                    <td    align="right"  width="80">'.number_format($ARH_PrimaPactada1,2).'</td>
                    <td    align="right" width="80" >'.number_format($ARH_PrimasPagadas1,2).'</td>
                    <td    align="right" width="120" >'.number_format($ARH_PrimaAdicionalPagada1,2).'</td>
                    <td    align="right" width="120" >'.number_format($total_ARH_RetirosEfectuados,2).'</td>
                   

                    
            
                </tr>
            </table>

        </td>
    
    </tr>


    <tr>
 

</tr>

 
    
    
 




    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr class="border_top_light">
                     
                    <td  width="60" align="center" > </td>
                    <td  width="80" align="center" > </td>
                    <td  width="80" align="center" ><b>TOTAL</b></td>
               
                    <td    align="right"  width="80" class="negrita" >'.number_format($total_ARH_PrimaPactada,2).'</td>
                    <td    align="right" width="80"  class="negrita">'.number_format($total_ARH_PrimasPagadas,2).'</td>
                    <td    align="right" width="120"  class="negrita">'.number_format($total_ARH_PrimaAdicionalPagada,2).'</td>
                    <td    align="right" width="120" class="negrita" >'.number_format($ARH_RetirosEfectuados1,2).'</td>
                   

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
    <td></td>
    </tr>

    <tr>
    <td></td>
    </tr>
 
 

 
</table>';









$t_datosDetalleMovimientosPeriodo = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DETALLE DE MOVIMIENTOS EN EL PERIODO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                     
                    <td   class="border_light_td negrita" width="80" > Fecha  </td>
                    <td align="center"    class="border_light_td negrita" width="130" > Alternativa de Rendimiento </td>
                    <td    class="border_light_td negrita" width="180" > Descripción del Movimiento </td>
                    <td   class="border_light_td negrita" width="80"> Monto </td>
                    <td   class="border_light_td negrita" width="80"> Titulos </td>
                    <td align="center" class="border_light_td negrita" width="80"> Precio de titulo </td> 

                    
            
                </tr>
            </table>

        </td>
    
    </tr>';




    for($i=0; $i<12; $i++){

    $t_datosDetalleMovimientosPeriodo.='
            <tr>
                <td colspan="5">
                    <table border="0" width="740">
                    <tr>
                            
                    <td     width="80" > '.${"DMP_Fecha".$i}.'  </td>
                    <td   width="130" >'.${"DMP_AlternativaRendimiento".$i}.'</td>
                    <td    width="180" >'.${"DMP_DescripcionMovimiento".$i}.'</td>
                    <td   align="right"  width="80"> '.${"DMP_Monto".$i}.'</td>
                    <td   align="right"  width="80"> '.${"DMP_Titulos".$i}.'</td>
                    <td   align="right" width="80">'.${"DMP_PrecioTitulo".$i}.'</td> 

                    
            
                </tr>
                    </table>

                </td>

            </tr>';

    }


    $tmp='
    <!--
    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    
                    <td     width="80" > 23/09/2019   </td>
                    <td   width="130" > SEG-SZ-FONSER1 </td>
                    <td    width="180" > Venta de unidades(-)</td>
                    <td   align="right"  width="80">  611.71</td>
                    <td   align="right"  width="80"> 10.91 </td>
                    <td   align="right" width="80"> 56.053545 </td> 

                    
            
                </tr>
            </table>

        </td>

    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="740">
                <tr>
                    
                    <td     width="80" > 23/09/2019   </td>
                    <td   width="130" >  </td>
                    <td    width="180" > Costo cobertura(-)</td>
                    <td   align="right"  width="80"> 787.40</td>
                    <td   align="right"  width="80">  </td>
                    <td   align="right" width="80">  </td> 

                    
            
                </tr>
            </table>

        </td>

    </tr>
    <tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr>
                
                <td     width="80" > 23/09/2019   </td>
                <td   width="130" >SEG-SZ-FONSER1</td>
                <td    width="180" > Venta de unidades(+)</td>
                <td   align="right"  width="80"> 787.40</td>
                <td   align="right"  width="80"> 14.05 </td>
                <td   align="right" width="80"> 56.053545 </td> 

                
        
            </tr>
        </table>

    </td>

</tr>

<tr>
<td colspan="5">
    <table border="0" width="740">
        <tr>
            
            <td     width="80" > 23/09/2019   </td>
            <td   width="130" > </td>
            <td    width="180" > Gasto de gestion (-)</td>
            <td   align="right"  width="80"> 102.30</td>
            <td   align="right"  width="80">   </td>
            <td   align="right" width="80">  </td> 

            
    
        </tr>
    </table>

</td>

</tr>

<tr>
<td colspan="5">
    <table border="0" width="740">
        <tr>
            
            <td     width="80" > 23/09/2019   </td>
            <td   width="130" > SEG-SZ-FORSER1 </td>
            <td    width="180" > Venta de unidades (+)</td>
            <td   align="right"  width="80"> 102.30</td>
            <td   align="right"  width="80"> 1.83 </td>
            <td   align="right" width="80">56.053545  </td> 

            
    
        </tr>
    </table>

</td>

</tr>



<tr>
<td colspan="5">
    <table border="0" width="740">
        <tr>
            
            <td     width="80" > 24/09/2019   </td>
            <td   width="130" > </td>
            <td    width="180" > Rescate parcial (-)</td>
            <td   align="right"  width="80"> 93,000.00</td>
            <td   align="right"  width="80">   </td>
            <td   align="right" width="80">  </td> 

            
    
        </tr>
    </table>

</td>

</tr>




<tr>
<td colspan="5">
    <table border="0" width="740">
        <tr>
            
            <td     width="80" > 24/09/2019   </td>
            <td   width="130" >SEG-SZ-FONSER1 </td>
            <td    width="180" > Venta de unidades (+)</td>
            <td   align="right"  width="80"> 93,000.00</td>
            <td   align="right"  width="80"> 1,658.78  </td>
            <td   align="right" width="80"> 56.065190 </td> 

            
    
        </tr>
    </table>

</td>

</tr>

<tr>
<td colspan="5">
    <table border="0" width="740">
        <tr>
            
            <td     width="80" > 24/09/2019   </td>
            <td   width="130" >  </td>
            <td    width="180" > Monto Impuesto ISR</td>
            <td   align="right"  width="80"> 119.58</td>
            <td   align="right"  width="80">   </td>
            <td   align="right" width="80">  </td> 

            
    
        </tr>
    </table>

</td>

</tr>

<tr>
<td colspan="5">
    <table border="0" width="740">
        <tr>
            
            <td     width="80" > 24/09/2019   </td>
            <td   width="130" > SEG-SZ-FONSER1 </td>
            <td    width="180" > Venta de unidades (+)</td>
            <td   align="right"  width="80"> 119.58</td>
            <td   align="right"  width="80"> 2.13  </td>
            <td   align="right" width="80"> 56.065190 </td> 

            
    
        </tr>
    </table>

</td>

</tr> -->
';

$t_datosDetalleMovimientosPeriodo.='

 
        </table>

    </td>

</tr>
    

    <tr>
    <td colspan="5">
        &nbsp;

    </td>

</tr>

<tr>
<td colspan="5">
    &nbsp;

</td>

</tr>


<tr>
<td colspan="5">
    &nbsp;

</td>

</tr>


 
    
    
 

 
 
</table>';






$t_datosDescripcionConceptos = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DESCRIPCION DE CONCEPTOS</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0"  >
                <tr>
                     
                    <td width="30%">Compra de títulos:</td><td width="70%">Indica el número de títulos que se adquirieron derivado del ingreso a la póliza.</td>
 
                </tr>

                <tr>
                        
                    <td width="30%">Costo de Cobertura:</td><td width="70%">Muestra el costo aplicado por concepto de mortalidad y gastos administrativos.</td>

                    
            
                </tr>
                <tr>
                        
                <td width="30%">Costo por administración de <br>fondo:</td><td width="70%">Refiere al cargo se aplica a la póliza por la administración de alternativas de Rendimiento.</td>

                
        
            </tr>
                <tr>
                        
                    <td width="30%">Gasto de gestión:</td><td width="70%">Indica los movimientos de cargo al fondo, tales como: derecho de póliza, administración de alternatvias de rendimiento, entre otros.</td>

                    
            
                </tr>

                <tr>
                        
                    <td width="30%">Ingreso por Ajuste a la Reserva:</td><td width="70%">Se realiza un ajuste a la reserva en favor del cliente, equivalente a las retenciones de impuestos a los que está sujeta la misma. <b>IMPORTANTE: Este ajuste no produce ningún efecto fiscal al cliente y se realiza a la reserva de la aseguradora.</b></td>

                    
            
                </tr>
                <tr>
                        
                    <td colspan="2">&nbsp;</td>
            
                </tr>

                <tr>
                        
                <td colspan="2" class="negrita">NOTA IMPORTANTE: La información contenida en el presente documento, es únicamente de carácter informativo.
                
                </td>
        
            </tr>
            </table>

        </td>
    
    </tr>
   
<tr>
<td colspan="5">
    &nbsp;

</td>

</tr>


 
    
    
 

 
 
</table>';















$htmlCuerpo4 =  $estilo .  $t_datosComisionesAplicadasPromedio . $t_datosAportacionesyRetirosHistorico;
$pdf->writeHTML($htmlCuerpo4, true, false, true, false, '');


$pdf->AddPage();


$htmlCuerpo5 = $estilo  .  $t_datosDetalleMovimientosPeriodo . $t_datosDescripcionConceptos;
$pdf->writeHTML($htmlCuerpo5, true, false, true, false, '');



// add a page
















 


// -------------------------------------------------------------------

//Close and output PDF document
ob_clean();
 $pdf->Output('example_009.pdf', 'I');
 end_ob_clean();
//echo site_url();

//============================================================+
// END OF FILE
//============================================================+