<?php
 
 
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

 

    //Page header
        //Page header
        public function Header() {
            // Logo
            $image_file =  base_url().'/images/zurich-logo.jpg';
            $this->Image($image_file, 15, 3, 80, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            // Set font
    
            $this->SetY(13);
            $this->SetFont('helvetica', 'B', 10);
           
            // Producto 
            $this->Cell(-25, 25, 'OBJETIVO PROTEGIDO SANTANDER', 0, false, 'R', 0, '', 0, false, 'M', 'M');
            $this->SetY(20);
            $this->SetFont('helvetica', 'B', 10);
            // Subtitle
             $this->Cell(0, 0, 'ESTADO DE CUENTA', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        }
 

    // Page footer
    public function Footer() {

        $this->SetY(-25);
        // Set font
        $this->SetFont('helvetica', 'B', 7);
        // Page number
        $this->Cell(0, 10, 'Av. Juan Salvador Agraz #73, pisos 3 y 4 Col. Santa Fe Cuajimalpa, Del.Cuajimalpa de Morelos, CP. 05348, CDMX, Mexico, Tel 51 69 43 00', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        $this->SetY(-22);
        // Set font
        $this->SetFont('helvetica', 'B', 7);
        // Page number 
        $this->Cell(0, 10, 'en la Cd. de México y área metropolitana o lada sin costo 01 800 501 0000 del interior de la República.', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('OBJETIVO PROTEGIDO SANTANDER');
$pdf->SetSubject('ESTADO DE CUENTA');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData('', 120, 'UNIT LINKED SANTANDER', 'ESTADO DE CUENTA');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// -------------------------------------------------------------------

// add a page
$pdf->AddPage();
// create some HTML content
$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';
 $pdf->SetFont('Helvetica', ' ', 10);


 

$estilo = '
    <style>
        tr.border_bottom td {
            border-bottom:1pt solid red;
        }


        tr.border_top td {
            border-top:1pt solid red;
          }

          tr.border_top_light td {
            border-top:0.5pt solid black;
          }

          td.border_top_light_td {
            border-top:0.5pt solid black;
          }
          td.border_light_td {
            border:0.5pt solid black;
          }

          td.border_top_light {
            border:0.5pt solid black;
           
          
          }

     


        tr.border_Subbottom td {
            border-bottom:0.5pt solid black;
          }


          span.smalltext {
            font-size: .8em; /* .8em x 10px = 8px */
          }

          td.smalltext10 {
            font-size: 10px; /* .8em x 10px = 8px */
          }

          td.smalltext {
            font-size: .8em; /* .8em x 10px = 8px */
          }

          
          span.normaltext {
            font-size: .12em; /* .8em x 10px = 8px */
          }

          td.normaltext {
            font-size: .12em; /* .8em x 10px = 8px */
          }

          td.negrita {
            font-weight: bold;
          }

 
    </style> ';

$titulo = '

    <table>
        <tr class="border_top"><td></td></tr>
  
        <tr>
        <td><h4>Zurich Santander Seguros México S.A.</h4></td></tr>
        <tr><td></td></tr>
    </table>'; 


 

$t_datosContratante = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="2"><b>DATOS DEL CONTRATANTE</b></td>
        <td colspan="2" align="right">PERIODO DEL <b>'.$datosPoliza['finicio'].'</b> AL <b>'.$datosPoliza['ffinal'].'</b></td>
        
    </tr>
    <tr >
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td align="left" class="smalltext10" >
            <table border="0" width="770">
                <tr>
                    
                    <td width="110"><b>CONTRATANTE:</b></td>
                    <td width="260">'.mb_strtoupper($ContratanteNombre).'</td>
                    <td width="170"><b>NÚMERO DE PÓLIZA:</b></td>
                    <td> 1 ' . $Ramo . ' ' . str_pad($noPoliza, 10, "0",STR_PAD_LEFT).'</td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr><td class="smalltext10"></td></tr>

    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="730">
                <tr>
                    
                    <td width="110"><b>ASEGURADO:</b></td>
                    <td width="260">'.mb_strtoupper($Asegurado).'</td>
                    <td width="170"><b>PRODUCTO:</b></td>
                    <td width="150">' . $Producto . '<br></td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5"  class="smalltext10">
            <table border="0" width="730">
                <tr>
                    
                    <td width="110"><b>DOMICILIO:</b></td>
                    <td width="240">'.mb_strtoupper($DireccionContratante,'utf-8').'</td>
                    <td width="20"> </td>
                    <td width="170"><b>MONEDA:</b></td>
                    <td>PESOS</td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5"  class="smalltext10">
            <table border="0" width="730">
                <tr>
                    
                    <td width="110"> </td>
                    <td width="260"> </td>
                    <td width="170"><b>FECHA DE EMISIÓN:</b></td>
                    <td>'.$datosPoliza['fechaEmision'].'<br></td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5"  class="smalltext10">
            <table border="0" width="730">
                <tr>
                    
                    <td width="110"> </td>
                    <td width="260"> </td>
                    <td width="170"><b>FECHA DE VENCIMIENTO:</b></td>
                    <td>'.format_dates($FechaVencimiento).'<br></td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5"  class="smalltext10">
            <table border="0" width="730">
                <tr>
                    
                    <td width="110"> </td>
                    <td width="260"> </td>
                    <td width="170"><b>FECHA DE CORTE:</b></td>
                    <td>'.$datosPoliza['ffinal'].'<br></td>
                </tr>
            </table>

        </td>
    </tr>
     
     
   
</table>';

$wd_ejecutivo_label = 110;
$wd_ejecutivo_data = 260;
$wd_correo_label = 160;
$wd_correo_data = 160;



//$EjecutivoNombre

$t_datosBanquero = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DATOS DEL BANQUERO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740" >
                <tr>
                  
                    <td width="110"><b>EJECUTIVO:</b></td>
                    <td width="260">'.mb_strtoupper($datosEjecutivo['NOMBRE_EJECUTIVO']).'<br></td>
                    <td width="160"><b>TELÉFONO:</b></td>
                    <td width="100">'.$datosEjecutivo['TELEFONO_1'].'</td>
                 
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740"  >
                <tr>
                   
                    <td width="'.$wd_ejecutivo_label.'"><b>CÓDIGO:</b><br></td>
                    <td width="'.$wd_ejecutivo_data.'">'.$datosEjecutivo['EXPEDIENTE'].'</td>
                    <td width="'.$wd_correo_label.'"><b>CORREO ELECTRÓNICO:</b></td>
                    <td width="'.$wd_correo_data.'">'.$datosEjecutivo['CORREO'].'</td>
                   
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740">
                <tr>
                 
                    <td width="'.$wd_ejecutivo_label.'"><b>SUCURSAL:</b></td>
                    <td width="'.$wd_ejecutivo_data.'">'.$datosEjecutivo['NOM_SUCURSAL'].'</td>
                    <td width="'.$wd_correo_data.'"> </td>
                    <td> </td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
     
     
   
</table>';

$t_coberturasAmparadas = '
<table border="0" width="770" bgcolor="">
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>COBERTURAS AMPARADAS</b></td>
        
        
    </tr>
    <tr>
       <td colspan="5" class="smalltext10">
          <table border="0" width="100%">
                <tr>
                    <td> </td>
                    <td> </td>
                    <td align="center"><b>Suma Asegurada</b></td>
                </tr>
            </table>
        </td>
    </tr>
 

    <tr>
    <td colspan="5" class="smalltext10">
        <table  width="740" border="0" >
                <tr>
                    <td width="3"></td>
                    <td></td>
                    <td width="305"> </td>
                    <td width="100" align="right" class="border_top_light_td"><b>Contratada</b></td>
                    <td width="100" align="right" class="border_top_light_td" ><b>Alcanzada</b></td>
                </tr>
            </table>
    </td>
</tr>

    <tr>
        <td colspan="5" class="smalltext10">
            <table    width="740" border="0" >
                    <tr class="border_top_light"> 
                        <td width="3"></td>
                        <td><b>Cobertura Básica:</b></td>
                        <td width="305">'.$CoberturaBasicaTxt.'</td>
                        <td width="100" align="right">'. number_format($CoberturaBasicaAlcanzada,2).'</td>
                        <td width="100" align="right">'. number_format($CoberturaBasicaContratada,2).'</td>
                    </tr>
                </table>
        </td>
    </tr>';


    if($CoberturaAdicionalContratada!=""){

    $t_coberturasAmparadas.= '
    <tr>
    <td colspan="5" class="smalltext10" >
        <table    width="740" border="0" >
                <tr class=""> 
                    <td width="3"></td>
                    <td><b>Coberturas Adicionales:</b></td>
                    <td width="305">'.$CoberturaAdicionalTxt.'</td>
                    <td width="100" align="right">'.number_format($CoberturaAdicionalAlcanzada,2).'</td>
                    <td width="100" align="right">'.number_format($CoberturaAdicionalContratada,2).'</td>
                </tr>
            </table>
    </td>
</tr>';

}


$t_coberturasAmparadas.= '
    
    <tr>
     <td colspan="5"></td>
    </tr>
        
 

 
     
     
   
</table>';


$listTitles = array_keys($datos_titulo);
sort($listTitles);

 

    $total_SAR_SaldoInicio  = 0;
    $total_SAR_Ingresos = 0;
    $total_SAR_CostoSeguro = 0;
    $total_SAR_OtrosEgresos = 0;
    $total_SAR_Rendimientos = 0;
    $total_SAR_SaldosCierre = 0;
 

   for($i=0;$i<count($listTitles);$i++){
    
    $saldoInicio =   $calculo_alternativa_rendim[$listTitles[$i]]['corte_inicio'] * $datos_titulo[$listTitles[$i]]['titulo_inicio'];
    $saldoAlCierre =   $calculo_alternativa_rendim[$listTitles[$i]]['corte_final'] * $datos_titulo[$listTitles[$i]]['titulo_final'];

    //$total_SAR_SaldoInicio = $total_SAR_SaldoInicio + $saldoInicio;
    $total_SAR_Ingresos = $total_SAR_Ingresos + $calculo_alternativa_rendim[$listTitles[$i]]['SAR_ingresos'];
    $total_SAR_CostoSeguro =  $total_SAR_CostoSeguro + $calculo_alternativa_rendim[$listTitles[$i]]['SAR_costo_seguro'];
    $total_SAR_OtrosEgresos = $total_SAR_OtrosEgresos + $calculo_alternativa_rendim[$listTitles[$i]]['SAR_otros_egresos'];

    $total_SAR_SaldoInicio  = $total_SAR_SaldoInicio + $saldoInicio;
    $total_SAR_SaldosCierre = $total_SAR_SaldosCierre + $saldoAlCierre;

    $SAR_Rendimientos = $saldoAlCierre - $saldoInicio - $calculo_alternativa_rendim[$listTitles[$i]]['SAR_ingresos'] + $calculo_alternativa_rendim[$listTitles[$i]]['SAR_costo_seguro'] +$calculo_alternativa_rendim[$listTitles[$i]]['SAR_otros_egresos'];

    $total_SAR_Rendimientos = $total_SAR_Rendimientos + $SAR_Rendimientos;
  

 


    }





$MP_SaldoInicio = $total_SAR_SaldoInicio;//$calculo_alternativa_rendim['corte_inicio'] * 1; //$datos_titulo['titulo_inicio'];

$MP_SaldoCierre = $total_SAR_SaldosCierre; // $calculo_alternativa_rendim['corte_final'] * 1;// $datos_titulo['titulo_final'];

$MP_Ingreso =   $total_SAR_Ingresos;


$SAR_SaldoInicio1 = $calculo_alternativa_rendim[$listTitles[0]]['corte_inicio'] *1; // $datos_titulo['titulo_inicio'];

$SAR_SaldosCierre1 = $calculo_alternativa_rendim[$listTitles[0]]['corte_final'] * 1; //$datos_titulo['titulo_final'];

$SAR_Ingresos1 = $calculo_alternativa_rendim[$listTitles[0]]['SAR_ingresos'];

//$SAR_Ingresos1 = $calculo_alternativa_rendim['ingresos'];
$SAR_CostoSeguro1 = $total_SAR_CostoSeguro;;
$SAR_OtrosEgresos1  = $total_SAR_OtrosEgresos;


$SAR_Rendimientos1 = $SAR_SaldosCierre1 - $SAR_SaldoInicio1 - $SAR_Ingresos1 + $SAR_CostoSeguro1  + $SAR_OtrosEgresos1;


$egresos_T = $calculo_alternativa_rendim[$listTitles[0]]['SAR_otros_egresos'] + $calculo_alternativa_rendim[$listTitles[0]]['SAR_costo_seguro'];


$MP_Ingreso = $MP_Aportaciones = $MP_IngresoAjusteReserva = $MP_Retiros = $MP_ISR = "0";

$MP_ISR = $calculo_alternativa_rendim[$listTitles[0]]['ISR'];
$MP_Retiros = $calculo_alternativa_rendim[$listTitles[0]]['retiros'];

// print_r($calculo_alternativa_rendim); die();
$total_SAR_OtrosEgresos = $total_SAR_OtrosEgresos - $MP_ISR - $MP_Retiros ;

//echo "otrosEgresos:" . $calculo_alternativa_rendim[$listTitles[0]]['SAR_otros_egresos']; die();


$totalEgresos = $SAR_CostoSeguro1 + $total_SAR_OtrosEgresos + $MP_Retiros +$MP_ISR;


$t_resumenPeriodo = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>RESUMEN DE MOVIMIENTOS DEL PERIODO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    
    <tr>
        <td width="80"></td>
        <td colspan="4" class="smalltext10">
            <table border="0" width="500">

                <tr class="border_Subbottom">
                        
                    <td width="250">Saldo al Inicio</td>
                    <td width="150" align="right">'.number_format($total_SAR_SaldoInicio,2).'</td>
            
                </tr>
                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(+) Ingreso </td>
                    <td width="150" align="right"> '.number_format($total_SAR_Ingresos,2).'</td>
                
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Aportaciones </td>
                    <td width="150" align="right">'.number_format($total_SAR_Ingresos,2).'</td>
            
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Ingreso por Ajuste a la Reserva </td>
                    <td width="150" align="right">'.number_format($MP_IngresoAjusteReserva,2).'</td>
        
                </tr>


                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(-) Egreso </td> 
                    <td width="150" align="right">'.number_format($totalEgresos,2).'</td>
                
                </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Costo del Seguro y Derecho de Póliza </td>
                    <td width="130" align="right">'.number_format($total_SAR_CostoSeguro,2).'</td>
        
                </tr>
                
                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Cargos a la Póliza</td>
                    <td width="130" align="right">'.number_format($total_SAR_OtrosEgresos,2).'</td>
    
                 </tr>

                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Retiros</td>
                    <td width="130" align="right"> '.number_format($MP_Retiros,2).'</td>
 
                </tr>


                <tr class="border_Subbottom">
                    
                        
                    <td width="270">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Impuesto Sobre la Renta</td>
                    <td width="130" align="right"> '.number_format($MP_ISR,2).'</td>
 
                </tr>
                <tr class="border_Subbottom">
                    
                    <td width="250">&nbsp;&nbsp;(+) Rendimientos </td>
                    <td width="150" align="right"> '.number_format($total_SAR_Rendimientos,2).'</td>
                
                </tr>


                <tr class="border_Subbottom">
                            
                    <td width="250">Saldo al Cierre**</td>
                    <td width="150" align="right"> '.number_format($MP_SaldoCierre,2).'</td>
        
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                

                
            </table>

        </td>

    </tr>



    <tr>
   
    <td colspan="4">
        <table border="0" width="600">
          <tr>
                <td class="smalltext" >
                        ** La Aseguradora con base en el Artículo 93, Fracción XXI de LISR pagará el rendimiento sin retención de impuesto, siempre y   </td>
            </tr>
            <tr>

            <td class="smalltext" >
                        
                        
                        
                        cuando el Asegurado cumpla con los siguiente: 
                        </td> 
            </tr>
            <tr>
                <td class="smalltext">
                            a) A la fecha de rescate tenga 60 años de edad o más.
                            <br>
                            b) Que hayan transcurrido al menos 5 años desde la fecha de contratación del seguro y el momento en que se pague el rescate.
                            <br>
                            c) Que el Asegurado sea el Contratante de la póliza.
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            
            <tr>
                <td class="smalltext">
                        La Aseguradora emitirá la constancia de retención de impuestos por el 20% sobre interés real cuando el Asegurado rescate su póliza, 
                </td>
                
                </tr>
            <tr>
                <td class="smalltext">
                 y no cumpla con los requisitos anteriormente citados.
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>



            <tr>
                <td class="smalltext">
                       Sujeto a lo previsto en la normatividad vigente al momento del pago del rendimiento.
                </td>
            </tr>
        </table>

    </td>

</tr>
<tr>
<td>&nbsp;</td>
</tr>


    

   

 
     
     
   
</table>';


$htmlCuerpo1 = $estilo . $titulo . $t_datosContratante . $t_datosBanquero . $t_coberturasAmparadas;

// output the HTML content
$pdf->writeHTML($htmlCuerpo1, true, false, true, false, '');




$ejecutivo_label = 130;
$ejecutivo_data = 150;
$correo_label = 160;
$correo_data = 180;

 


/////////////////////////////////////////////////////////////////////////////////////////////////  PAGE 2




//print_r($datos_titulo); die();
//print_r(array_keys($datos_titulo)); die();
/*
    [0] => SEG_SZ_DEUDA_OP
    [1] => SEG_SZ_STERUSD
    [2] => SEG_SZ_STERGOB

*/



//print_r($listTitles); die();

 

 //echo $datos_titulo['SEG_SZ_DEUDA_OP']['titulo_inicio']; die();

 //echo $listTitles[0]; die();

 //echo $datos_titulo[$listTitles[0]]['titulo_inicio']; die();




$t_datosResumenRendimientosAlternativa = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>RESUMEN DE RENDIMIENTOS</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740">
                <tr>
                    <td width="32"  > </td>
                    <td align="center" class="border_top_light" ><b>Alternativa de Rendimiento </b> </td>
                    <td align="center" class="border_top_light"><b>Distribución de Portafolio </b> <sup>(1)</sup></td>
                    <td class="border_top_light" >
                        <table   cellspacing=0 style="width: 100%; padding:0px; margin: 0px;">
                       
                            <tr>
                                <td colspan="2"   align="center"  ><b>Precio Titulo</b></td>
                            </tr>
                            <tr>
                                <td align="center"  ><b>Inicio</b></td><td align="center" ><b>Fin</b></td>
                            </tr>
                        </table>
                    </td>

                    <td align="center" width="190" class="border_top_light">
                        <b>Tasa de Rendimiento </b> <sup>(2)</sup>
                    </td>
            
                </tr>
            </table>

        </td>
    
    </tr>

    
   
<tr>
 
 
<td colspan="5" class="smalltext10">


    <table border="0" width="740">';

    $array_porcentaje = array();
    $TotalsaldoAlCierre = 0;


    for($i=0;$i<count($listTitles);$i++){

        $array_porcentaje [$listTitles[$i]]= array( $calculo_alternativa_rendim[$listTitles[$i]]['corte_final'] * $datos_titulo[$listTitles[$i]]['titulo_final']);
        $TotalsaldoAlCierre = $TotalsaldoAlCierre + $calculo_alternativa_rendim[$listTitles[$i]]['corte_final'] * $datos_titulo[$listTitles[$i]]['titulo_final'];
       
  
     }

    

   /*  print_r($array_porcentaje); 


   
     die();*/
 





 


for($i=0;$i<count($listTitles);$i++){

   
    $tasa_anual = "";
    if($datos_titulo[$listTitles[$i]]['tasa']=="FIJA") {
        $tasa_anual = $datos_titulo[$listTitles[$i]]['anual'];
        $tasa = "(a)";
    }else{
        $tasa_anual = $datos_titulo[$listTitles[$i]]['efectiva'];
        $tasa = "(b)";
    }

   // $tasa = "(a)";
   //<td align="center">'.number_format($calculo_alternativa_rendim['porcentaje_titulo'],2).'%</td>

    $t_datosResumenRendimientosAlternativa.=       '<tr>
            <td width="32"> </td>
            <td  >'.$listTitles[$i].'</td>';

          


if(count($listTitles>1)){
            
            $t_datosResumenRendimientosAlternativa.= '<td align="center">'.number_format(($array_porcentaje[$listTitles[$i]][0]/$TotalsaldoAlCierre)*100,2).'%</td>';
        }

        else{
                    
            $t_datosResumenRendimientosAlternativa.= '<td align="center">100%</td>';
    

        }
            $t_datosResumenRendimientosAlternativa.=       '<td>
                <table>
                
                    <tr>
                        <td align="center">'.number_format($datos_titulo[$listTitles[$i]]['titulo_inicio'],6).'</td><td align="center">'.number_format($datos_titulo[$listTitles[$i]]['titulo_final'],6).'</td>
                    </tr>
                </table>
            </td>

            <td align="right">
               '.number_format($tasa_anual,2).' % <sup>'.$tasa.'</sup>
                            </td>
    
        </tr>';

}
 
$t_datosResumenRendimientosAlternativa.= '</table>

</td> 

</tr>
<tr>
<td colspan="5"> </td>
       
</tr>

<tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr>
                
                <td>
                    <table border="0">
                        <tr>
                            <td  class="smalltext" width="20">   
                                (1) 
                            </td>
                            <td class="smalltext" width="650" >
                                Porcentaje de distribución de Saldo a la fecha de corte, correspondiente a la proporción de cada una de la alternativas de rendimiento.
                            
                            </td>


                        </tr>

                        <tr>
                            <td  class="smalltext" width="20">   
                                (2)  
                            </td>
                            <td class="smalltext"  width="600">
                                (a) Tasa efectiva anual en el periodo para Alternativas de renta fija. (b) Rendimiento directo para alternativas en renta variable.
                            
                            </td>

                        </tr>
                
                
                
                                
                    </table>
                
                </td>
        
            </tr>
        </table>

    </td>

</tr>

<tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
</table>';

 
/*
$total_TAR_Inicio = $TAR_Inicio1 + $TAR_Inicio2;
$total_TAR_ingresos = $TAR_Ingresos1 + $TAR_Ingresos2;
$total_TAR_costoSeguro = $TAR_CostosSeguro1 + $TAR_CostosSeguro2;
$total_TAR_otrosEgresos = $TAR_OtrosEgresos1 + $TAR_OtrosEgresos2;
$total_TAR_TitulosCierre = $TAR_TitulosCierre1 + $TAR_TitulosCierre2;*/


$t_datosAlternativasRendimiento = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>TÍTULOS DE ALTERNATIVAS DE RENDIMIENTO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740">
                <tr>
                     
                    <td align="center"  width="120"  class="border_top_light" ><b>Alternativa de Rendimiento </b> </td>
                    <td align="center"   width="120"  class="border_top_light"><b>% Distribución de Ingresos</b></td>
                    <td align="center" class="border_top_light"><b>Inicio</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(+) Ingresos</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Costos del Seguro</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Otros Egresos</b></td>
                    <td align="center" class="border_top_light" width="100"><b>Titulos al Cierre</b></td>

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5" class="smalltext10">

       
       
            <table border="0" width="740">'; 

            $total_TAR_ingresos = 0;
            $total_TAR_costoSeguro = 0;

          //print_r($calculo_alternativa_rendim); die();

            for($i=0;$i<count($listTitles);$i++){


                $total_TAR_Inicio = $total_TAR_Inicio + $calculo_alternativa_rendim[$listTitles[$i]]['corte_inicio'];
                $total_TAR_ingresos = $total_TAR_ingresos + $calculo_alternativa_rendim[$listTitles[$i]]['TAR_ingresos'];
                $total_TAR_costoSeguro = $total_TAR_costoSeguro + $calculo_alternativa_rendim[$listTitles[$i]]['TAR_costo_seguro'];
                $total_TAR_otrosEgresos = $total_TAR_otrosEgresos + $calculo_alternativa_rendim[$listTitles[$i]]['TAR_otros_egresos'];
                $total_TAR_TitulosCierre = $total_TAR_TitulosCierre + $calculo_alternativa_rendim[$listTitles[$i]]['corte_final'];//$total_TAR_TitulosCierre + $calculo_alternativa_rendim[$listTitles[$i]]['titulo_cierre'];

                $t_datosAlternativasRendimiento.='<tr class="">
                        
                        <td  width="120"  >'.$listTitles[$i].'</td>
                        <td  align="right" >'.number_format($datos_titulo[$listTitles[$i]]['porcentaje'],2) .'%</td>
                        <td align="right"  >'.number_format($calculo_alternativa_rendim[$listTitles[$i]]['corte_inicio'],2).'</td>
                        <td    width="70" align="right">'.number_format($calculo_alternativa_rendim[$listTitles[$i]]['TAR_ingresos'],2).'</td>
                        <td    width="70" align="right">'.number_format($calculo_alternativa_rendim[$listTitles[$i]]['TAR_costo_seguro'],2).'</td>
                        <td    width="70" align="right">'.number_format($calculo_alternativa_rendim[$listTitles[$i]]['TAR_otros_egresos'],2).'</td>
                        <td    width="100" align="right">'.number_format($calculo_alternativa_rendim[$listTitles[$i]]['corte_final'],2).'</td>

                        
                
                    </tr>';
            
            }

                
                $t_datosAlternativasRendimiento.='</table>
      

        </td>
    
    </tr>

    <tr>
    <td colspan="5" class="smalltext10">
        <table border="0" width="740"  >
            <tr  class="border_top_light">
                 
                   
            <td  width="120"  >  </td>
            <td  align="right"  ><b>TOTAL TITULOS</b></td>
                
                <td align="right" class="negrita"  > '.number_format($total_TAR_Inicio,2).'</td>
                <td    width="70" class="negrita"  align="right">'.number_format($total_TAR_ingresos,2).'</td>
                <td    width="70" align="right" class="negrita" >'.number_format($total_TAR_costoSeguro,2).'</td>
                <td    width="70" align="right" class="negrita" >'.number_format($total_TAR_otrosEgresos,2).'</td>
                <td    width="100" align="right" class="negrita" > '.number_format($total_TAR_TitulosCierre ,2).' </td>

                
        
            </tr>
        </table>

    </td>

</tr>

    
    

<tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>
 
 

 
</table>';




$total_SAR_SaldoInicio = $SAR_SaldoInicio1 + $SAR_SaldoInicio2 + $SAR_SaldoInicio3 + $SAR_SaldoInicio4 + $SAR_SaldoInicio5 +  $SAR_SaldoInicio6 + $SAR_SaldoInicio7;
$total_SAR_Ingresos = $SAR_Ingresos1 + $SAR_Ingresos2 + $SAR_Ingresos3 + $SAR_Ingresos4 + $SAR_Ingresos5 + $SAR_Ingresos6 + $SAR_Ingresos7;
$total_SAR_CostoSeguro = $SAR_CostoSeguro1 + $SAR_CostoSeguro2 + $SAR_CostoSeguro3 + $SAR_CostoSeguro4 + $SAR_CostoSeguro5 + $SAR_CostoSeguro6 + $SAR_CostoSeguro7;
$total_SAR_OtrosEgresos = $SAR_OtrosEgresos1 + $SAR_OtrosEgresos2 + $SAR_OtrosEgresos3 + $SAR_OtrosEgresos4 + $SAR_OtrosEgresos5 + $SAR_OtrosEgresos6 + $SAR_OtrosEgresos7;
$total_SAR_Rendimientos = $SAR_Rendimientos1 + $SAR_Rendimientos2 + $SAR_Rendimientos3 + $SAR_Rendimientos4 + $SAR_Rendimientos5 + $SAR_Rendimientos6 + $SAR_Rendimientos7;
$total_SAR_SaldosCierre = $SAR_SaldosCierre1 + $SAR_SaldosCierre2 + $SAR_SaldosCierre3 + $SAR_SaldosCierre4 + $SAR_SaldosCierre5 + $SAR_SaldosCierre6 + $SAR_SaldosCierre7;
 


$t_datosAlternativasRendimientoMXN = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>SALDO DE ALTERNATIVAS DE RENDIMIENTO(MXN)</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740">
                <tr>
                     
                    <td align="center"  width="120"  class="border_top_light" ><b>Alternativa de Rendimiento </b> </td>
                    <td align="center"   width="120"  class="border_top_light"><b>Saldo al Inicio</b></td>
                    <td align="center" class="border_top_light"><b>(+) Ingresos</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Costos del Seguro</b></td>
                    <td align="center" class="border_top_light" width="70"><b>(-) Otros Egresos</b></td>
                    <td align="center" class="border_top_light" width="100"><b>(+) Rendimientos</b></td>
                    <td align="center" class="border_top_light" width="100"><b>Saldos al Cierre</b></td>

                    
            
                </tr>
            </table>

        </td>
    
    </tr>';

 
    $total_SAR_SaldoInicio  = 0;
    $total_SAR_Ingresos = 0;
    $total_SAR_CostoSeguro = 0;
    $total_SAR_OtrosEgresos = 0;
    $total_SAR_Rendimientos = 0;
    $total_SAR_SaldosCierre = 0;
 

   for($i=0;$i<count($listTitles);$i++){

    $saldoInicio =   $calculo_alternativa_rendim[$listTitles[$i]]['corte_inicio'] * $datos_titulo[$listTitles[$i]]['titulo_inicio'];
    $saldoAlCierre =   $calculo_alternativa_rendim[$listTitles[$i]]['corte_final'] * $datos_titulo[$listTitles[$i]]['titulo_final'];
     
    $total_SAR_SaldoInicio = $total_SAR_SaldoInicio + $saldoInicio;
    $total_SAR_Ingresos = $total_SAR_Ingresos + $calculo_alternativa_rendim[$listTitles[$i]]['SAR_ingresos'];
    $total_SAR_CostoSeguro =  $total_SAR_CostoSeguro + $calculo_alternativa_rendim[$listTitles[$i]]['SAR_costo_seguro'];
    $total_SAR_OtrosEgresos = $total_SAR_OtrosEgresos + $calculo_alternativa_rendim[$listTitles[$i]]['SAR_otros_egresos'];

  //  $total_SAR_SaldoInicio  = $total_SAR_SaldoInicio + $saldoInicio;
    $total_SAR_SaldosCierre = $total_SAR_SaldosCierre + $saldoAlCierre;

    $SAR_Rendimientos = $saldoAlCierre - $saldoInicio - $calculo_alternativa_rendim[$listTitles[$i]]['SAR_ingresos'] + $calculo_alternativa_rendim[$listTitles[$i]]['SAR_costo_seguro'] +$calculo_alternativa_rendim[$listTitles[$i]]['SAR_otros_egresos'];

    $total_SAR_Rendimientos = $total_SAR_Rendimientos + $SAR_Rendimientos;
      
   
        $t_datosAlternativasRendimientoMXN.='<tr>
         
        
                <td colspan="5" class="smalltext10">
                    <table border="0" width="740">
                        <tr class="">
                            
                            <td  width="120"  >'.$listTitles[$i].'</td>
                            <td  align="right" >'.number_format($saldoInicio,2).'</td>
                            <td align="right"  >'.number_format($calculo_alternativa_rendim[$listTitles[$i]]['SAR_ingresos'],2).'</td>
                            <td    width="70" align="right">'.number_format($calculo_alternativa_rendim[$listTitles[$i]]['SAR_costo_seguro'],2).'</td>
                            <td    width="70" align="right">'.number_format($calculo_alternativa_rendim[$listTitles[$i]]['SAR_otros_egresos'],2).'</td>
                            <td    width="100" align="right">'.number_format($SAR_Rendimientos,2).'</td>
                            <td    width="100" align="right">'.number_format($saldoAlCierre,2).'</td>
                            
                            
                    
                        </tr>
                    
        
                    </table>


                </td>

            
            
            </tr>';

    }


    $t_datosAlternativasRendimientoMXN.='
    <tr>
    <td colspan="5" class="smalltext10">
        <table border="0" width="740">
            <tr class="border_top_light" >
                 
                <td  width="120"  ><b>TOTAL SALDO</b></td>
                <td  align="right" class="negrita" >'.number_format($total_SAR_SaldoInicio,2).'</td>
                <td align="right" class="negrita" >'.number_format($total_SAR_Ingresos,2).'</td>
                <td    width="70" align="right" class="negrita">'.number_format($total_SAR_CostoSeguro,2).'</td>
                <td    width="70" align="right" class="negrita">'.number_format($total_SAR_OtrosEgresos,2).'</td>
                <td    width="100" align="right" class="negrita">'.number_format($total_SAR_Rendimientos,2).'</td>
                <td    width="100" align="right" class="negrita">'.number_format($total_SAR_SaldosCierre,2).'</td>

                
        
            </tr>
        </table>

    </td>

</tr>
    
    

<tr>
        <td colspan="5">
            &nbsp;

        </td>
    
    </tr>

    <tr>
    <td colspan="5">
        <table border="0" width="740">
            <tr>
                
                <td>
                    <table border="0">
                        <tr>
                            <td  class="smalltext" width="20">   
                                (3) 
                            </td>
                            <td class="smalltext" width="650" >
                               Porcentaje actual de distribución de ingresos, el cual es utilizado para distribuir las aportaciones de prima recibidos en la póliza.
                            </td>


                        </tr>

                        <tr>
                            <td  class="smalltext" width="20">   
                                (4)  
                            </td>
                            <td class="smalltext"  width="600">
                               Cargos aplicados por concepto mortalidad, costo de administración y derecho de póliza, tal como se específica en las condiciones generales del producto.
                            </td>

                        </tr>
                

                        <tr>
                        <td  class="smalltext" width="20">   
                            (5)  
                        </td>
                        <td class="smalltext"  width="600">
                         Retiros efectuados a petición del Contratante, impuestos, costo de administración de cada alternativa de rendimiento y cargos generados por movimientos realizados por el cliente.                    </td>

                    </tr>
            
                
                
                                
                    </table>
                
                </td>
        
            </tr>
        </table>

    </td>

</tr>
 <tr>
 <td>&nbsp;</td>
 </tr>

 
</table>';



// add a page
$pdf->AddPage();









$htmlCuerpo2 = $estilo .  $t_resumenPeriodo. $t_datosResumenRendimientosAlternativa . $t_datosAlternativasRendimiento;

$pdf->writeHTML($htmlCuerpo2, true, false, true, false, '');





// add a page
$pdf->AddPage();





$t_datosPresentarSolicitudDeAclaracionoServicio = '
<table border="0" width="770"  >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>PARA PRESENTAR UNA SOLICITUD DE ACLARACIÓN O SERVICIO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
    <td colspan="5">
            <table border="0" width="100%">
                <tr>
                    
                    <td>
                        <table border="0"   >
                            <tr>
                               
                                <td class="smalltext" width="*" >
                                <b>UNIDAD ESPECIALIZADA PARA LA ATENCIÓN DE USUARIOS (UEA)</b>
                                <br>
                                <br>
                                Av. Juan Salvador Agraz #73, piso 3, Col. Santa Fe Cuajimalpa, Del. Cuajimalpa de Morelos, C.P. 05348, Ciudad de México
                                <br> 
                                Correo Electrónico: ueaseguros@santander.com.mx
                                <br>
                                <br>
                                Teléfonos: 55 1037-3500 Ext. 13566, 13571 y 13515, con un horario de atención de lunes a jueves de 7:30 a 17:00 horas y viernes 
                                <br>
                                de 7:30 a 14:00 horas.
                                <br>
                                <br>
                                <b>CONDUSEF</b>
                                <br>
                                <br>
                                Av. Insurgentes Sur #762, planta baja, Col. Del Valle, Del. Benito Juárez, C.P.03100, Ciudad de México.
                                <br>
                                Correo Electrónico: asesoria@condusef.gob.mx, Página Web: http://www.condusef.gob.mx, Teléfonos: 01 800 999 8080 o 5340 0999 
                                <br>
                                

                                </td>


                            </tr>
 
                
                    
                    
                                    
                        </table>
                    
                    </td>
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5"> </td>
       
    </tr>



</table>';



    $t_datosComisionesAplicadasPromedio = '

    <table border="0" width="770" >
        
        <tr class="border_bottom">
            <td width="40"> </td>
            <td colspan="4"><b>COMISIONES PROMEDIO APLICADAS DE SIEFORES Y FONDOS DE INVERSIÓN</b></td>
        </tr>


        <tr>
        <td colspan="5"> </td>
        </tr>
        <tr>
        <td colspan="5"> </td>
        </tr>
        
        
        <tr>

        
        <td  width="750" colspan=5 >



<table width="100%" border="0">

<tr>

  <td width="200" class="smalltext10"><table width="100%" border="0">

  <tr>
  <td colspan="2" class="smalltext" align="center"> Comisiones de las Siefores Básicas (Al cierre de Diciembre de 2017) <br>
    </td>
  </tr>

    <tr bgcolor="#333333">

      <td width="56%" class="smalltext"><div align="center" ><br><br><br><font color="White"><b> Afore</b></font></div></td>

      <td width="44%"><table width="100%" border="1">

        <tr>

          <td class="smalltext" height="60"><div align="center">  <br><br><font color="white"><b>Porcentaje</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext"><div align="center"><font color="white"><b>Anual</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext"><div align="center"><font color="white"><b>Sobre Saldo</b></font></div></td>

        </tr>

      </table></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>AZTECA</b></td>

      <td class="smalltext border_light_td" align="center" >1.100%</td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>BANAMEX</b></td>

      <td class="smalltext border_light_td"><div align="center">0.990%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>COPPEL</b></td>

      <td class="smalltext border_light_td"><div align="center">1.100%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>INBURSA</b></td>

      <td class="smalltext border_light_td"><div align="center">0.980%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>INVERCAP</b></td>

      <td class="smalltext border_light_td"><div align="center">1.100%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>METLIFE</b></td>

      <td class="smalltext border_light_td"><div align="center">1.100%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>PENSION ISSSTE</b></td>

      <td class="smalltext border_light_td"><div align="center">0.860%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>PRINCIPAL</b></td>

      <td class="smalltext border_light_td"><div align="center">1.090%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>PROFUTURO GNP</b></td>

      <td class="smalltext border_light_td"><div align="center">1.030%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>SURA</b></td>

      <td class="smalltext border_light_td"><div align="center">1.030%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td"><b>XXI BANORTE</b></td>

      <td class="smalltext border_light_td"><div align="center">1.000%</div></td>

    </tr>

    <tr>

      <td class="smalltext border_light_td" bgcolor="black" align="center"><b><font color="white">PROMEDIO</font></b></td>

      <td class="smalltext border_light_td"  bgcolor="black"><div align="center"><b><font color="white">1.035%</font></b></div></td>

    </tr>

  </table></td>

  <td width="33">&nbsp;</td>

  <td width="400"><table  width="100%" border="0">

    <tr>

      <td class="smalltext" align="center"> Comisiones de los Productos de Ahorro IV 5 <br>

        (Al Cierre de Diciembre de 2017)<br></td>

    </tr>

    <tr>

      <td class="smalltext"><table width="100%" border="1">

        <tr bgcolor="#333333">

          <td class="smalltext" width="60"><div align="center"><br><br><br><font color="white"><b>Número de fondo</b></font></div></td>

          <td class="smalltext" width="230"><div align="center"><br><br><br><br><font color="white"><b>Nombre del fondo</b></font></div></td>

          <td class="smalltext" width="50"><div align="center"><font color="white"><b>Porcentaje sobre el saldo antes de la renovación.</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext" align="center">1</td>

          <td class="smalltext">FONDO SANTANDER IVS 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="center">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext"  align="center">3</td>

          <td class="smalltext">FONDO SANTANDER IVS 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="center">1.100%</div></td>

        </tr>

        <tr>

          <td class="smalltext"  align="center">4</td>

          <td class="smalltext">FONDO SANTANDER IVS 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="center">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext"  align="center">6</td>

          <td class="smalltext">FONDO SANTANDER IVS 1 - RENTA FIJA</td>

          <td class="smalltext"><div align="center">1.150%</div></td>

        </tr>

        <tr>

          <td class="smalltext" bgcolor="black" >&nbsp;</td>

          <td class="smalltext"  bgcolor="black" align="center"><b><font color="white">PROMEDIO</font></b></td>

          <td class="smalltext" bgcolor="black" ><div align="center"><b><font color="white">1.063%</font></b></div></td>

        </tr>

        <tr>

          <td class="smalltext" align="center">2</td>

          <td class="smalltext">FONDO SANTANDER IVS 2 - MIXTO</td>

          <td class="smalltext"><div align="center">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext" align="center">5</td>

          <td class="smalltext">FONDO SANTANDER IVS 2 - MIXTO</td>

          <td class="smalltext"><div align="center">1.000%</div></td>

        </tr>

        <tr>

          <td class="smalltext" bgcolor="black" >&nbsp;</td>

          <td class="smalltext"  align="center" bgcolor="black"><b><font color="white">PROMEDIO</font></b></td>

          <td class="smalltext" bgcolor="black"  align="center" > <b><font color="white">1.000%</font></b> </td>

        </tr>

      </table></td>

    </tr>

    <tr>

      <td class="smalltext" align="center"> &nbsp;</td>

    </tr>

    <tr>

    <td class="smalltext" align="center"> &nbsp;</td>

  </tr>
  <tr>

  <td class="smalltext" align="center"> &nbsp;</td>

</tr>

    <tr>

      <td class="smalltext"><table width="100%" border="0">

        <tr bgcolor="#333333">

          <td class="smalltext"  width="60"><div align="center"><font color="white"><br><b>Sociedad de Inversión</b></font> </div></td>

          <td class="smalltext"  width="230"><div align="center"><font color="white"><br><b>EMISORA</b></font></div></td>

          <td class="smalltext"  width="60"><div align="center"  ><font color="white"><b>Porcentaje Anual Sobre Saldo</b></font></div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">IXELQ BF2</td>

          <td class="smalltext border_light_td">FONDO DE BANORTEIXE (GUB/PRIVADOS)</td>

          <td class="smalltext border_light_td"><div align="center">1.500%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">BMER180 B2</td>

          <td class="smalltext border_light_td">FONDO DE BBVA BANCOMER (GUB/PRIVADOS)</td>

          <td class="smalltext border_light_td"><div align="center">1.939%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">BMER30 B2</td>

          <td class="smalltext border_light_td">FONDO DE BBVA BANCOMER (GUB/PRIVADOS)</td>

          <td class="smalltext border_light_td"><div align="center">1.939%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">BMER90 B2</td>

          <td class="smalltext border_light_td">FONDO DE BBVA BANCOMER (GUB/PRIVADOS)</td>

          <td class="smalltext border_light_td"><div align="center">1.939%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td" bgcolor="black">&nbsp;</td>

          <td class="smalltext border_light_td" bgcolor="black"><div align="center"><b><font color="white">PROMEDIO</font></b></div></td>

          <td class="smalltext border_light_td"  bgcolor="black" align="center"><b><font color="white">1.829%</b></font></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">ACTIVCO B-2</td>

          <td class="smalltext border_light_td">FONDO DE ACTINVER (RENTA FIJA/VARIABLE)</td>

          <td class="smalltext border_light_td"><div align="center">1.068%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">DIVER-CP</td>

          <td class="smalltext border_light_td">FONDO DE BBVA BANCOMER (RENTA FIJA/VARIABLE)</td>

          <td class="smalltext border_light_td"><div align="center">1,989%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td">SURV10 BF2</td>

          <td class="smalltext border_light_td">FONDO DE SURA(RENTA FIJA/VARIABLE)</td>

          <td class="smalltext border_light_td"><div align="center">1.700%</div></td>

        </tr>

        <tr>

          <td class="smalltext border_light_td" bgcolor="black">&nbsp;</td>

          <td class="smalltext border_light_td" align="center" bgcolor="black"><b><font color="white">PROMEDIO</font></b></td>

          <td class="smalltext border_light_td"  align="center"  bgcolor="black"><b><font color="white"> 1.586%</font></b> </td>

        </tr>

      </table></td>

    </tr>

    <tr>

      <td class="smalltext">&nbsp; </td>

    </tr>

    <tr>

      <td class="smalltext">&nbsp;</td>

    </tr>

  </table></td>

</tr>

  </table></td>

</tr>

</table>


          
        
        </td>
        
        </tr>
    
    

    
    </table>';


$htmlCuerpo3 = $estilo . $t_datosAlternativasRendimientoMXN . $t_datosPresentarSolicitudDeAclaracionoServicio;

$pdf->writeHTML($htmlCuerpo3, true, false, true, false, '');




$pdf->AddPage();


//////////////////////////////////////////////////////////////////////////////////  Page 4

#ARH_PrimaPactada1

 //print_r($calculo_alternativa_rendim); die();
// print_r($calculo_alternativa_rendim); die();

$ARH_PrimaPactada1 = number_format($PRIMA_PACTADA,2);
$ARH_PrimasPagadas1 = number_format($calculo_alternativa_rendim[$listTitles[0]]['aporte_monto']+$calculo_alternativa_rendim[$listTitles[0]]['aporte_adicional'],2);
$ARH_RetirosEfectuados1 =  number_format($calculo_alternativa_rendim[$listTitles[0]]['retiros'],2);
$ARH_PrimaAdicionalPagada1 = number_format(0,2);

$total_ARH_PrimaPactada = $ARH_PrimaPactada1; // $ARH_PrimaPactada1 + $ARH_PrimaPactada2 + $ARH_PrimaPactada3 + $ARH_PrimaPactada4 + $ARH_PrimaPactada5;
$total_ARH_PrimasPagadas = $ARH_PrimasPagadas1; //$ARH_PrimasPagadas1 + $ARH_PrimasPagadas2 + $ARH_PrimasPagadas3 + $ARH_PrimasPagadas4 + $ARH_PrimasPagadas5;
$total_ARH_PrimaAdicionalPagada = $ARH_PrimaAdicionalPagada1;// + $ARH_PrimaAdicionalPagada2 + $ARH_PrimaAdicionalPagada3 + $ARH_PrimaAdicionalPagada4 + $ARH_PrimaAdicionalPagada5;
$total_ARH_RetirosEfectuados = $ARH_RetirosEfectuados1; // + $ARH_RetirosEfectuados2 + $ARH_RetirosEfectuados3 + $ARH_RetirosEfectuados4 + $ARH_RetirosEfectuados5;

$ARH_AnioPoliza1 = 1;

$t_datosAportacionesyRetirosHistorico = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>APORTACIONES Y RETIROS (HISTORICO)</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740">
                <tr>
                     
                    <td align="center"  class="border_top_light" width="60" ><b>Año Póliza</b> </td>
                    <td align="center"  class="border_top_light" width="80" ><b>Fecha Desde</b></td>
                    <td align="center"  class="border_top_light" width="80" ><b>Fecha Hasta</b></td>
                    <td align="center" class="border_top_light" width="80"><b>Prima Pactada</b></td>
                    <td align="center" class="border_top_light" width="80"><b>Primas Pagadas</b></td>
                    <td align="center" class="border_top_light" width="120"><b>Prima Adicional Pagada</b></td>
                    <td align="center" class="border_top_light" width="120"><b>Retiros Efectuados</b></td>

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740">
                <tr class=" ">
                     
                    <td  width="60" align="center" >'.$ARH_AnioPoliza1.'</td>
                    <td  width="80" align="center" >'.format_dates($calculo_alternativa_rendim[$listTitles[0]]['aporte_fecha']).'</td>
                    <td  width="80" align="center" >'.$datosPoliza['ffinal'].'</td>
               
                    <td    align="right"  width="80">'.$ARH_PrimaPactada1.'</td>
                    <td    align="right" width="80" >'.$ARH_PrimasPagadas1.'</td>
                    <td    align="right" width="120" >'.$ARH_PrimaAdicionalPagada1.'</td>
                    <td    align="right" width="120" >'.$ARH_RetirosEfectuados1.'</td>
                   

                    
            
                </tr>
            </table>

        </td>
    
    </tr>


    <tr>
 

</tr>

 
    
    
 




    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740">
                <tr class="border_top_light">
                     
                    <td  width="60" align="center" > </td>
                    <td  width="80" align="center" > </td>
                    <td  width="80" align="center" ><b>TOTAL</b></td>
               
                    <td    align="right"  width="80" class="negrita" >'.$total_ARH_PrimaPactada.'</td>
                    <td    align="right" width="80"  class="negrita">'.$total_ARH_PrimasPagadas.'</td>
                    <td    align="right" width="120"  class="negrita">'.$ARH_PrimaAdicionalPagada1.'</td>
                    <td    align="right" width="120" class="negrita" >'.$ARH_RetirosEfectuados1.'</td>
                   

                    
            
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
    <td></td>
    </tr>

    <tr>
    <td></td>
    </tr>
 
 

 
</table>';








$t_datosDetalleMovimientosPeriodo = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4"><b>DETALLE DE MOVIMIENTOS EN EL PERIODO</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5" class="smalltext10">
            <table border="0" width="740">
                <tr>
                     
                    <td   class="border_light_td negrita" width="80" > Fecha  </td>
                    <td align="center"    class="border_light_td negrita" width="130" > Alternativa de Rendimiento </td>
                    <td    class="border_light_td negrita" width="180" > Descripción del Movimiento </td>
                    <td   class="border_light_td negrita" width="80"> Monto </td>
                    <td   class="border_light_td negrita" width="80"> Títulos </td>
                    <td align="center" class="border_light_td negrita" width="80"> Precio de títulos </td> 

                    
            
                </tr>
            </table>

        </td>
    
    </tr>';



    foreach($detalle_movimiento_periodo as $row){

  
                       
           $titulo = ""; 
           
           
 

    

        $tag = "";

            if($row['SEG_SZ_STERGOB']!=""){
                $tag = "SEG_SZ_STERGOB";

                $titulo = number_format(quitCommas($row['SEG_SZ_STERGOB']),2);

            }

      

            if($row['SEG_SZ_STEREAL']!=""){
                $tag = "SEG_SZ_STEREAL";
                $titulo = number_format(quitCommas($row['SEG_SZ_STEREAL']),2);

            }

            if($row['SEG_SZ_USA_EQ']!=""){
                $tag = "SEG_SZ_USA_EQ";
                $titulo = number_format(quitCommas($row['SEG_SZ_USA_EQ']),2);

            }


            if($row['SEG_SZ_STER_OP']!=""){
                $tag = "SEG_SZ_STER_OP";
                $titulo = number_format(quitCommas($row['SEG_SZ_STER_OP']),2);


            }


            if($row['SEG_SZ_STERUSD']!=""){
                $tag = "SEG_SZ_STERUSD";
                $titulo = number_format(quitCommas($row['SEG_SZ_STERUSD']),2);


            }


            if($row['SEG_SZ_EURO_EQ']!=""){
                $tag = "SEG_SZ_EURO_EQ";
                $titulo = number_format(quitCommas($row['SEG_SZ_EURO_EQ']),2);


            }


            if($row['SEG_SZ_FONSER']!=""){
                $tag = "SEG_SZ_FONSER";
                $titulo = number_format(quitCommas($row['SEG_SZ_FONSER']),2);



            }


            if($row['SEG_SZ_DEUDA_OP']!=""){
                $tag = "SEG_SZ_DEUDA_OP";
                $titulo = number_format(quitCommas($row['SEG_SZ_DEUDA_OP']),2);



            }


    if($titulo == "0.00"){
            $titulo = "";
        }




     



        $precio = number_format($row['PRECIO'],6);
        if($precio == "0.000000"){
            $precio = "";
        }
        
        




        $t_datosDetalleMovimientosPeriodo.='
            <tr>
                <td colspan="5" class="smalltext10">
                    <table border="0" width="740">
                    <tr>
                            
                    <td     width="80" > '.format_dates($row['FECHA']).'  </td>
                    <td   width="130" >'.$tag.'</td>
                    <td    width="180" >'.$row['MOVIMIENTO'].'</td>
                    <td   align="right"  width="80"> '.number_format(floatNum($row['MONTO']),2).'</td>
                    <td   align="right"  width="80"> '.$titulo.'</td>
                    <td   align="right" width="80">'.$precio.'</td> 

                    
            
                </tr>
                    </table>

                </td>

            </tr>';

        }
    //}


  

$t_datosDetalleMovimientosPeriodo.='

 
        </table>

    </td>

</tr>
    

    <tr>
    <td colspan="5">
        &nbsp;

    </td>

</tr>

<tr>
<td colspan="5">
    &nbsp;

</td>

</tr>


<tr>
<td colspan="5">
    &nbsp;

</td>

</tr>


 
    
    
 

 
 
</table>';






$t_datosDescripcionConceptos = '
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="4" align="center"><b>DESCRIPCIÓN DE CONCEPTOS</b></td>
        
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5" class="smalltext">
            <table border="0"  >
                <tr>
                     
                    <td width="30%">Compra de Títulos:</td><td width="70%">Indica el número de títulos que se adquirieron derivado del ingreso a la póliza.</td>
 
                </tr>

                <tr>
                        
                    <td width="30%">Costo de Cobertura:</td><td width="70%">Muestra el costo aplicado por concepto de mortalidad y gastos administrativos.</td>

                    
            
                </tr>
                <tr>
                        
                <td width="30%">Costo por administración de <br>fondo:</td><td width="70%">Refiere al cargo se aplica a la póliza por la administración de alternativas de Rendimiento.</td>

                
        
            </tr>
                <tr>
                        
                    <td width="30%">Gasto de gestión:</td><td width="70%">Indica los movimientos de cargo al fondo, tales como: derecho de póliza, administración de alternatvias de rendimiento, entre otros.</td>

                    
            
                </tr>

                <tr>
                        
                    <td width="30%">Ingreso por Ajuste a la Reserva:</td><td width="70%">Se realiza un ajuste a la reserva en favor del cliente, equivalente a las retenciones de impuestos a los que está sujeta la misma. <b>IMPORTANTE: Este ajuste no produce ningún efecto fiscal al cliente y se realiza a la reserva de la aseguradora.</b></td>

                    
            
                </tr>
                <tr>
                        
                    <td colspan="2">&nbsp;</td>
            
                </tr>

                <tr>
                        
                <td colspan="2" class="negrita">NOTA IMPORTANTE: La información contenida en el presente documento, es únicamente de carácter informativo.
                
                </td>
        
            </tr>
            </table>

        </td>
    
    </tr>
   
<tr>
<td colspan="5">
    &nbsp;

</td>

</tr>


 
    
    
 

 
 
</table>';















$htmlCuerpo4 =  $estilo .  $t_datosComisionesAplicadasPromedio . $t_datosAportacionesyRetirosHistorico;
$pdf->writeHTML($htmlCuerpo4, true, false, true, false, '');


$pdf->AddPage();


$htmlCuerpo5 = $estilo  .  $t_datosDetalleMovimientosPeriodo . $t_datosDescripcionConceptos;
$pdf->writeHTML($htmlCuerpo5, true, false, true, false, '');



// add a page
















 


// -------------------------------------------------------------------

//Close and output PDF document
ob_clean();

$finicio = reverse_format_dates($datosPoliza['finicio']);
$final = reverse_format_dates($datosPoliza['ffinal']);


/*
$fileName = $noPoliza."_".$finicio."__".$final.".pdf";
$fileName =  "OP-1".$Ramo.$noPoliza."_".date("Ymd")."(".$finicio."_".$final.").pdf";
*/
$fileName = $noPoliza."_".$finicio."__".$final.".pdf";
$fileName =  "OP-1".$Ramo.$noPoliza."_".date("Ymd").".pdf";
 

if($datosPoliza['display']=="yes"){
   

  $pdf->Output($fileName, 'I');
  $pdf->Output('example_009.pdf', 'I');
 end_ob_clean();

}else{
//echo site_url();

$new_folder = date("Y-m-d");
//echo $new_folder;

if (!file_exists('/var/www/html/zurich_edodecuenta/pdfs/'.$new_folder)) {
    mkdir('/var/www/html/zurich_edodecuenta/pdfs/'.$new_folder, 0777, true);
}


 //$filelocation = "/var/www/project/custom"; //Linux

 $fileNL = "/var/www/html/zurich_edodecuenta/pdfs/".$new_folder."/".$fileName; //Linux

 $pdf->Output($fileNL, 'F');

echo "Ok";

}

 /*
 ob_clean();
 $pdf->Output($fileName, 'I');
 end_ob_clean();*/