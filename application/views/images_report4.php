<?php


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
 

    // Page footer
    public function Footer() {

        $this->SetY(-25);
        // Set font
        $this->SetFont('helvetica', ' ', 8);
        // Page number
        $this->Cell(0, 10, 'Av. Juan Salvador Agraz #73, pisos 3 y 4 Col. Santa Fe Cuajimalpa, Del.Cuajimalpa de Morelos, CP. 05348, CDMX, Mexico, Tel 51', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        $this->SetY(-22);
        // Set font
        $this->SetFont('helvetica', ' ', 8);
        // Page number
        $this->Cell(0, 10, '69 43 00 en la Cd. de Mexico y area meropolitana o lada sin costo 01 800 501 0000 del interior de la Republica.', 0, false, 'C', 0, '', 0, false, 'T', 'M');



        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('UNIT LINKED SANTANDER');
$pdf->SetSubject('ESTADO DE CUENTA');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'UNIT LINKED SANTANDER', 'ESTADO DE CUENTA');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// -------------------------------------------------------------------

// add a page
$pdf->AddPage();
// create some HTML content
$subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';
 $pdf->SetFont('helvetica', ' ', 10);

$html = '

<style>
tr.border_bottom td {
    border-bottom:1pt solid black;
  }
</style>



<br><h4>Zurich Santander Seguros México S.A.</h4>
<table border="0" width="770" >
    <tr class="border_bottom">
        <td width="40"> </td>
        <td colspan="2"><b>DATOS DEL CONTRATANTE</b></td>
        <td colspan="2" align="right">PERIODO DEL: <b>01/10/2019</b> AL <b>31/10/2019</b></td>
        
    </tr>
    <tr>
        <td colspan="5"> </td>
       
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>CONTRATANTE:</b></td>
                    <td width="220">ANTONIO</td>
                    <td width="150"><b>NUMERO DE POLIZA:</b></td>
                    <td>1 102 00000012020</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>ASEGURADO:</b></td>
                    <td width="220">ANTONIO</td>
                    <td width="150"><b>PRODUCTO:</b></td>
                    <td>UNIT LINKED SATANDER</td>
                </tr>
            </table>

        </td>
    
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"><b>DOMICILIO:</b></td>
                    <td width="220">SONORA</td>
                    <td width="150"><b>MONEDA:</b></td>
                    <td>PESOS</td>
                </tr>
            </table>

        </td>
    
    </tr>
    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE EMISION:</b></td>
                    <td>24/05/2019</td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE VENCIMIENTO:</b></td>
                    <td>19/05/2019</td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td colspan="5">
            <table border="0" width="730">
                <tr>
                    <td width="10"> </td>
                    <td width="130"> </td>
                    <td width="220"> </td>
                    <td width="150"><b>FECHA DE CORTE:</b></td>
                    <td>31/10/2019</td>
                </tr>
            </table>

        </td>
    </tr>
     
     
    <tr>
        <td>1</td>
        <td bgcolor="#cccccc" align="center" colspan="2">A1 ex<i>amp</i>le <a href="http://www.tcpdf.org">link</a> column span. One two tree four five six seven eight nine ten.<br />line after br<br /><small>small text</small> normal <sub>subscript</sub> normal <sup>superscript</sup> normal  bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla<ol><li>first<ol><li>sublist</li><li>sublist</li></ol></li><li>second</li></ol><small color="#FF0000" bgcolor="#FFFF00">small small small small small small small small small small small small small small small small small small small small</small></td>
        <td>4B</td>
    </tr>
    <tr>
        <td>'.$subtable.'</td>
        <td bgcolor="#0000FF" color="yellow" align="center">A2 € &euro; &#8364; &amp; è &egrave;<br/>A2 € &euro; &#8364; &amp; è &egrave;</td>
        <td bgcolor="#FFFF00" align="left"><font color="#FF0000">Red</font> Yellow BG</td>
        <td>4C</td>
    </tr>
    <tr>
        <td>1A</td>
        <td rowspan="2" colspan="2" bgcolor="#FFFFCC">2AA<br />2AB<br />2AC</td>
        <td bgcolor="#FF0000">4D</td>
    </tr>
    <tr>
        <td>1B</td>
        <td>4E</td>
    </tr>
    <tr>
        <td>1C</td>
        <td>2C</td>
        <td>3C</td>
        <td>4F</td>
    </tr>
</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// Print some HTML Cells

$html = '<span color="red">red</span> <span color="green">green</span> <span color="blue">blue</span><br /><span color="red">red</span> <span color="green">green</span> <span color="blue">blue</span>';

$pdf->SetFillColor(255,255,0);

$pdf->writeHTMLCell(0, 0, '', '', $html, 'LRTB', 1, 0, true, 'L', true);
$pdf->writeHTMLCell(0, 0, '', '', $html, 'LRTB', 1, 1, true, 'C', true);
$pdf->writeHTMLCell(0, 0, '', '', $html, 'LRTB', 1, 0, true, 'R', true);


// -------------------------------------------------------------------

//Close and output PDF document
ob_clean();
 $pdf->Output('example_009.pdf', 'I');
 end_ob_clean();
//echo site_url();

//============================================================+
// END OF FILE
//============================================================+